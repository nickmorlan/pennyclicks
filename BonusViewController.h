//
//  BonusViewController.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/21/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>


@interface BonusViewController : UIView <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property UITableView *table;

@end
