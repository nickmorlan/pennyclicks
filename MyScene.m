////
//  MyScene.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "MyScene.h"
#import "Penny.h"
#import "InfoLayer.h"
#import "GameModel.h"
#import "ViewController.h"
#import "iAdHelper.h"

@implementation MyScene
{
    Penny *_penny;
    SKAction *_moveFadeAndRemoveAction, *_waitAndRemoveAction, *_bagDropAction;
    long _pennyFractionConverter;
    int _extraPennyFraction, _clickMultiplierDuration, _clickMultiplierInterval, _clickMultiplierStopInterval, _newsScrollerInterval, _bonusMultiplierFactor, _pennyBagDropInterval;
    BOOL _inBonus, _initialLoad;
    InfoLayer *_infoLayer;
    GameModel *_gameModel;
    SKSpriteNode *_upgradesButton, *_statisticsButton, *_bonusButton, *_swirl, *_swirlBG, *_sudoBG;
    CFTimeInterval _lastStatRefresh, _lastClickMultiplier, _lastPennyBagDrop, _lastNewsSroller;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        // achievements
        //self.achievementsDictionary = [[NSMutableDictionary alloc] init];
        
        
        /* Setup your scene here */
        self.backgroundColor = [SKColor colorWithRed:0.15 green:1. blue:0.1 alpha:1.0];
        
        // set up label action to be called on clicks
        SKAction *move = [SKAction moveByX:0 y:130. duration:1.5];
        move.timingMode = SKActionTimingEaseOut;
        SKAction *fade = [SKAction fadeOutWithDuration:1.5];
        fade.timingMode = SKActionTimingEaseIn;
        SKAction *remove = [SKAction removeFromParent];
        SKAction *group = [SKAction group:@[move, fade]];
        _moveFadeAndRemoveAction = [SKAction sequence:@[group, remove]];
        
        // set up label action to be called on bonus rounds
        SKAction *wait = [SKAction waitForDuration:2];
        SKAction *fade1 = [SKAction fadeOutWithDuration:1];
        //fade1.timingMode = SKActionTimingEaseIn;
        _waitAndRemoveAction = [SKAction sequence:@[wait, fade1, remove]];
        
        // set up action for bag drop
        SKAction *move1 = [SKAction moveByX:0 y:-self.size.height duration:1];
        _bagDropAction = [SKAction sequence:@[move1, remove]];
        
        // backgrounds
        _swirl = [SKSpriteNode spriteNodeWithImageNamed:@"swirl"];
        _swirl.position = CGPointMake(CGRectGetMidX(self.frame),
                                      CGRectGetMidY(self.frame));
        [_swirl setScale:.4];
        _swirl.color = [SKColor yellowColor];
        _swirl.colorBlendFactor = .25;
        SKAction *action = [SKAction rotateByAngle:M_PI duration:15];
        [_swirl runAction:[SKAction repeatActionForever:action]];
        
        _swirlBG = [SKSpriteNode spriteNodeWithImageNamed:@"swirlbg"];
        _swirlBG.position = CGPointMake(CGRectGetMidX(self.frame),
                                      CGRectGetMidY(self.frame));
        _swirlBG.color = [SKColor colorWithRed:.8 green:1. blue:.74 alpha:1.];
        _swirlBG.colorBlendFactor = 1;
        SKAction *action1 = [SKAction rotateByAngle:- M_PI duration:5];
        // sudo bg is used as a color mask in the far background
        [_swirlBG runAction:[SKAction repeatActionForever:action1]];
        _sudoBG = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:CGSizeMake(self.frame.size.width, self.frame.size.height)];
        _sudoBG.position = CGPointMake(CGRectGetMidX(self.frame),
                                      CGRectGetMidY(self.frame));
        [self addChild:_sudoBG];
        [self addChild:_swirlBG];
        [self addChild:_swirl];
        
        // set up time to run pps calculation
        _pennyFractionConverter = 0;
        [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(penniesPerSecondCalculator:) userInfo:nil repeats:YES];
        
        // initialize GameModel instance
        _gameModel = [GameModel sharedManager];
        //debug
        //[_gameModel addToPennyBank:(unsigned long long)10000];
        
        // set up penny
        if(_gameModel.doubleClicker) {
            _penny = [[Penny alloc ]initWithDoubleClicker];
        } else {
            _penny = [[Penny alloc ]init];
            // add observer for double click purchase to show double penny
            NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
            [center addObserverForName:@"com.r5kmedia.PennyClicks.doubleclicker"
                                object:nil
                                 queue:nil
                            usingBlock:^(NSNotification *notification)
             {
                 NSLog(@"fghghfjfhj");
                 [self doubleClickerPurchased];
                 [[NSNotificationCenter defaultCenter] removeObserver:center];
             }];
        }
        _penny.name = @"penny";
        _penny.position = CGPointMake(CGRectGetMidX(self.frame),
                                      CGRectGetMidY(self.frame));
        [self addChild:_penny];
        // set up top info layer
        _infoLayer = [[InfoLayer alloc] initForScene:self.frame];
        [_infoLayer setPennyBankTo:_gameModel.pennyBank];
        [self addChild:_infoLayer];
        
        // set up buttons
        SKTextureAtlas *assets = [SKTextureAtlas atlasNamed:@"assets"];
        _upgradesButton = [SKSpriteNode spriteNodeWithTexture:[assets textureNamed:@"buttonupgrades"]];
        _upgradesButton.position = CGPointMake(self.frame.size.width / 2, 50);
        _upgradesButton.name = @"buttonUpgrades";
        _upgradesButton.anchorPoint = CGPointMake(.5, 0);
        
        [self addChild:_upgradesButton];

        _statisticsButton = [SKSpriteNode spriteNodeWithTexture:[assets textureNamed:@"buttonstatistics"]];
        _statisticsButton.position = CGPointMake(self.frame.size.width / 4, 50);
        _statisticsButton.name = @"buttonStatistics";
        _statisticsButton.anchorPoint = CGPointMake(.5, 0);
        [self addChild:_statisticsButton];

        _bonusButton = [SKSpriteNode spriteNodeWithTexture:[assets textureNamed:@"buttonbonus"]];
        _bonusButton.position = CGPointMake((self.frame.size.width / 4) * 3, 50);
        _bonusButton.name = @"buttonBonus";
        _bonusButton.anchorPoint = CGPointMake(.5, 0);
        [self addChild:_bonusButton];
        
        // preload sounds
        self.coinSound = [SKAction playSoundFileNamed:@"coin.mp3" waitForCompletion:NO];
        self.clickSound = [SKAction playSoundFileNamed:@"click.wav" waitForCompletion:NO];
        self.cashRegisterSound = [SKAction playSoundFileNamed:@"cashregister.mp3" waitForCompletion:NO];
        //self.audioPlayer = [[AVAudioPlayer alloc] init];
    
        
        // initialize bonus settings
        _clickMultiplierInterval = 30 + arc4random_uniform(30);
        _clickMultiplierDuration = 5 + arc4random_uniform(5);
        _newsScrollerInterval = 0;
        _bonusMultiplierFactor = 0;
        // add banner
        [iAdHelper sharedHelper];

        
        // add observer for  purchases to play caching
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserverForName:@"playCashRegister"
                            object:nil
                             queue:nil
                        usingBlock:^(NSNotification *notification)
         {
             [self runAction:self.cashRegisterSound];
         }];
        
        _inBonus = NO;
        _initialLoad = YES;
}
    return self;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        SKSpriteNode *node = (SKSpriteNode *)[self nodeAtPoint:location];
        if([node.name isEqualToString:@"penny"]) {
            _penny.touched = YES;
            [_penny setScale:.55];
        } else if ([node.name isEqualToString:@"buttonUpgrades"]) {
            [self upgradesButtonClick];
        } else if ([node.name isEqualToString:@"buttonStatistics"]) {
            [self statisticsButtonClick];
        } else if ([node.name isEqualToString:@"buttonBonus"]) {
            [self bonusButtonClick];
        } else if([node.name isEqualToString:@"bag"]) {
            [self doPennyBag:location forBag:node];
        }
    }

}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch ends */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        
        if([_penny isTouched]) {
            [_penny setTouched:NO];
            [_penny setScale:.6];
            SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"AmericanTypewriter-CondensedBold"];
            SKLabelNode *labelShadow = [SKLabelNode labelNodeWithFontNamed:@"AmericanTypewriter-CondensedBold"];
            label.fontSize = 30;
            labelShadow.fontSize = 30;
            labelShadow.fontColor = [UIColor blackColor];
            // set variation in initial positioning of label
            int offsetX = arc4random_uniform(81) - 40;
            int offsetY = arc4random_uniform(81) - 40;
            label.position = CGPointMake(location.x + offsetX, location.y + offsetY);
            labelShadow.position = CGPointMake(location.x + offsetX + 1, location.y + offsetY -+1);
            // add points
            if (_gameModel.clickMultiplier > 1) {
                label.text = [NSString stringWithFormat:@"+%d", _gameModel.clickMultiplier ];
                labelShadow.text = [NSString stringWithFormat:@"+%d", _gameModel.clickMultiplier ];
                [_gameModel addToPennyBank:_gameModel.clickMultiplier];
                [_gameModel addToLifetimePenniesFromClicks:_gameModel.clickMultiplier];
                [_gameModel incrementClicksSinceLastUpdateBy:_gameModel.clickMultiplier];
           } else {
                label.text = @"+1";
                labelShadow.text = @"+1";
                [_gameModel incrementPennyBank];
                [_gameModel incrementClicksSinceLastUpdate];
               [_gameModel addToLifetimePenniesFromClicks:1];
           }
            [_gameModel incrementTotalClicks];
            [_infoLayer setPennyBankTo:_gameModel.pennyBank];
            [self addChild:labelShadow];
            [self addChild:label];
            [label runAction:_moveFadeAndRemoveAction];
            [labelShadow runAction:_moveFadeAndRemoveAction];
            [self runAction:self.coinSound];
            
       }
        
    }
}


-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
    //NSLog(@"%f", currentTime);
    
    if(_initialLoad) {
        _initialLoad = NO;
        _clickMultiplierInterval = currentTime + 5 + arc4random_uniform(30);
    }
    if (currentTime - _lastStatRefresh > 2) {
        [_gameModel updateUpgradeStats];
        ViewController *controller = (ViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
        [controller updateStatisticsTable];
        _lastStatRefresh = currentTime;
    }
    
    // check bonus round
    [self checkBonusRoundLogic:currentTime];
    
    // news scroller control
    if (currentTime - _newsScrollerInterval > 10) {
        NSLog(@"%f - %d", currentTime, _newsScrollerInterval);
        [_infoLayer setupNewScrollerTextFromPPS:_gameModel.basePenniesPerSecond];
        _newsScrollerInterval = currentTime + 10;
    }
    
    // drop penny
    if (_pennyBagDropInterval < currentTime) {
        // set up a money bag to drop from the top of the screeen
        SKSpriteNode *bag = [SKSpriteNode spriteNodeWithImageNamed:@"moneybags"];
        bag.anchorPoint = CGPointMake(0, 0);
        int offsetX = arc4random_uniform(self.size.width - bag.size.width);
        bag.position = CGPointMake(offsetX, self.size.height);
        bag.name = @"bag";
       // NSLog(@"%f, %f", bag.position.x, bag.position.y);
        [self addChild:bag];
        [bag runAction:_bagDropAction];
        _pennyBagDropInterval = currentTime + arc4random_uniform(45) + 5;
    }
    
}

/* called 10 times per second... */
-(void)penniesPerSecondCalculator:(NSTimer *)timer
{
    [self doPennyCalculations];
    [self doInfoLayerUpdates];
}

-(void) doPennyCalculations
{
    if(_gameModel.basePenniesPerSecond < 1000) {
        // add whole base pennies per second
        // multiply by 10 to remove decimal
        _pennyFractionConverter += (int) floor(_gameModel.basePenniesPerSecond * 10);
        // add the whole pennies to the bank // 1 penny = 100 units
        if (_pennyFractionConverter >= 100) {
            //NSLog(@"%d", (int) _pennyFractionConverter / 100);
            int penniesToAdd = (int)floor(_pennyFractionConverter / 100);
            [_gameModel addToPennyBank:penniesToAdd];
            // convert back to 1 whole penny = 100 units
            _pennyFractionConverter -= (penniesToAdd * 100);
        }
        
        
        // grab the remainder fraction
        int extraFraction = (int) floor(_gameModel.basePenniesPerSecond) % 10;
        //NSLog(@"%d", (int)floor(_pennyFractionConverter / 10));
        // add the extra penny when neccesary
        _extraPennyFraction += extraFraction;
        if(_extraPennyFraction >= 10) {
            _extraPennyFraction -= 10;
            [_gameModel incrementPennyBank];
        }
    } else {
        // once the pennies per second is high enough just forget the fraction
        [_gameModel addToPennyBank:(unsigned long long) floor(_gameModel.basePenniesPerSecond)];
    }

        
}

-(void) doInfoLayerUpdates
{
    // handle clicks per second calculation..
    // use factor of 10 to stretch out 1/10 of a second to give a rough estamate
    [_gameModel.clicksPerPastSeconds addObject:[NSNumber numberWithInt:(_gameModel.clicksSinceLastUpdate * 10)]];
    if([_gameModel.clicksPerPastSeconds count] > 25) {
        [_gameModel.clicksPerPastSeconds removeObjectAtIndex:0];
    }
    NSNumber *average = [_gameModel.clicksPerPastSeconds valueForKeyPath:@"@avg.self"];
    //NSLog(@"%f", [average floatValue] + _gameModel.basePenniesPerSecond);
    // update the info layer display
    [_infoLayer setPenniesPerSecondTo:([average floatValue] + _gameModel.basePenniesPerSecond)];
    [_infoLayer setPennyBankTo:_gameModel.pennyBank];
    
    // update control vars
    [_gameModel resetClicksSinceLastUpdate];

}

-(void)upgradesButtonClick
{
    [self runAction:self.clickSound];
    ViewController *controller = (ViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [controller showUpgradesTable];
    
}

-(void)statisticsButtonClick
{
    [self runAction:self.clickSound];
    ViewController *controller = (ViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [controller showStatisticsTable];
    
}

-(void)bonusButtonClick
{
    [self runAction:self.clickSound];
    ViewController *controller = (ViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [controller showBonusTable];
  
    
}

-(void) doPennyBag:(CGPoint)location forBag:(SKSpriteNode *)node
{
    [self runAction:self.cashRegisterSound];
    NSNumber *value = [_gameModel getPennyBagDropValue];
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"AmericanTypewriter-CondensedBold"];
    SKLabelNode *labelShadow = [SKLabelNode labelNodeWithFontNamed:@"AmericanTypewriter-CondensedBold"];
    label.fontSize = 40;
    labelShadow.fontSize = 40;
    labelShadow.fontColor = [UIColor blackColor];
    label.position = location;
    labelShadow.position = CGPointMake(location.x + 2, location.y - 2);
    label.text = [NSString stringWithFormat:@"+%llu", [value unsignedLongLongValue]];
    labelShadow.text = [NSString stringWithFormat:@"+%llu", [value unsignedLongLongValue]];
    
    
    [self addChild:labelShadow];
    [self addChild:label];
    [label runAction:_moveFadeAndRemoveAction];
    [labelShadow runAction:_moveFadeAndRemoveAction];
    
    [_gameModel addToPennyBank:[value unsignedLongLongValue]];
    [node removeFromParent];
}

-(void) checkBonusRoundLogic:(CFTimeInterval)currentTime
{
    // bounus mulitplier round
    if (_clickMultiplierInterval < currentTime) {
        [_gameModel setRandomClickModifier];
        _gameModel.inBonus = YES;
        // bonous round label
        SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-CondensedBlack"];
        SKLabelNode *labelShadow = [SKLabelNode labelNodeWithFontNamed:@"HelveticaNeue-CondensedBlack"];
        label.fontSize = 50;
        labelShadow.fontSize = 50;
        labelShadow.fontColor = [UIColor blackColor];
        label.position = CGPointMake(CGRectGetMidX(self.frame),
                                    CGRectGetMidY(self.frame) + self.frame.size.height / 4);
        labelShadow.position = CGPointMake(CGRectGetMidX(self.frame) + 3,
                                           (CGRectGetMidY(self.frame) + self.frame.size.height / 4) - 3);
        if(_gameModel.doubleClicker) {
            label.text = [NSString stringWithFormat:@"Bonus x%d", _gameModel.clickMultiplier / 2];
            labelShadow.text = [NSString stringWithFormat:@"Bonus x%d", _gameModel.clickMultiplier / 2];
        } else {
            label.text = [NSString stringWithFormat:@"Bonus x%d", _gameModel.clickMultiplier];
            labelShadow.text = [NSString stringWithFormat:@"Bonus x%d", _gameModel.clickMultiplier];
         }
        
        [self addChild:labelShadow];
        [self addChild:label];
        [label runAction:_waitAndRemoveAction];
        [labelShadow runAction:_waitAndRemoveAction];
        
        [self updateBackgroundAnimation];
        
        // set random timing
        _clickMultiplierInterval = currentTime + 30 + arc4random_uniform(30) + _clickMultiplierDuration;
        _clickMultiplierStopInterval = currentTime + _clickMultiplierDuration + 3;
        
    }
    // end bonus round...
    if (_clickMultiplierStopInterval < currentTime) {
        // reset controls
        if (_gameModel.doubleClicker == YES) {
            [_gameModel setClickMultiplier:2];
        } else {
            [_gameModel setClickMultiplier:1];
        }
        _gameModel.inBonus = NO;
        
        [self restoreDefaultBackgroundAnimation];
        
        // set random timing
        _clickMultiplierInterval = currentTime + 30 + arc4random_uniform(30) + _clickMultiplierDuration;
        _clickMultiplierStopInterval = _clickMultiplierInterval + 10;
    }
    

}

-(void) updateBackgroundAnimation
{
    SKAction *action, *action1, *action2, *speed;
    if (_gameModel.clickMultiplier == 50) {
        action = [SKAction colorizeWithColor:[UIColor blackColor] colorBlendFactor:.75 duration:3];
        action1 = [SKAction colorizeWithColor:[UIColor blackColor] colorBlendFactor:.5 duration:3];
        action2 = [SKAction colorizeWithColor:[UIColor yellowColor] colorBlendFactor:.75 duration:3];
        speed = [SKAction speedBy:15 duration:3];
        speed.timingMode = SKActionTimingEaseIn;
    } else if (_gameModel.clickMultiplier == 20) {
        action = [SKAction colorizeWithColor:[UIColor cyanColor] colorBlendFactor:.5 duration:3];
        action1 = [SKAction colorizeWithColor:[UIColor cyanColor] colorBlendFactor:.25 duration:3];
        action2 = [SKAction colorizeWithColor:[UIColor magentaColor] colorBlendFactor:.75 duration:3];
        speed = [SKAction speedBy:12 duration:3];
        speed.timingMode = SKActionTimingEaseIn;
    }else if (_gameModel.clickMultiplier == 3) {
        action = [SKAction colorizeWithColor:[UIColor redColor] colorBlendFactor:.5 duration:3];
        action1 = [SKAction colorizeWithColor:[UIColor redColor] colorBlendFactor:.25 duration:3];
        action2 = [SKAction colorizeWithColor:[UIColor purpleColor] colorBlendFactor:.75 duration:3];
        speed = [SKAction speedBy:7 duration:3];
        speed.timingMode = SKActionTimingEaseIn;
    }else {
        action = [SKAction colorizeWithColor:[UIColor blueColor] colorBlendFactor:.5 duration:3];
        action1 = [SKAction colorizeWithColor:[UIColor blueColor] colorBlendFactor:.25 duration:3];
        action2 = [SKAction colorizeWithColor:[UIColor redColor] colorBlendFactor:.75 duration:3];
        speed = [SKAction speedBy:5 duration:3];
        speed.timingMode = SKActionTimingEaseIn;
    }
    [_sudoBG runAction:action2];
    [_swirl runAction:action];
    [_swirl runAction:speed];
    [_swirlBG runAction:action1];
    [_swirlBG runAction:speed];
}

-(void) restoreDefaultBackgroundAnimation
{
    SKAction *action = [SKAction colorizeWithColor:[UIColor yellowColor] colorBlendFactor:1 duration:3];
    SKAction *action1 = [SKAction colorizeWithColor:[UIColor colorWithRed:.8 green:1. blue:.74 alpha:1.] colorBlendFactor:1 duration:3];
    SKAction *action2 = [SKAction colorizeWithColor:[UIColor clearColor] colorBlendFactor:1 duration:3];
    SKAction *speed = [SKAction speedBy:.2 duration:3];
    speed.timingMode = SKActionTimingEaseOut;
    [_sudoBG runAction:action2];
    [_swirl runAction:action];
    [_swirl runAction:speed];
    [_swirlBG runAction:action1];
    [_swirlBG runAction:speed];
}

-(void) doubleClickerPurchased
{
    [_penny removeFromParent];
    _penny = nil;
    
    _penny = [[Penny alloc ]initWithDoubleClicker];
    _penny.name = @"penny";
    _penny.position = CGPointMake(CGRectGetMidX(self.frame),
                                  CGRectGetMidY(self.frame));
    [self addChild:_penny];
    
}
@end


