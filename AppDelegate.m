//
//  AppDelegate.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "AppDelegate.h"
#import "GameModel.h"
#import "STTwitter.h"
@import Accounts;

@implementation AppDelegate

@synthesize window, player;

#pragma mark -
#pragma mark Game Center Support

@synthesize currentPlayerID,
gameCenterAuthenticationComplete;

#pragma mark -
#pragma mark Game Center Support

// Check for the availability of Game Center API.
static BOOL isGameCenterAPIAvailable()
{
    // Check for presence of GKLocalPlayer API.
    Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
    
    // The device must be running running iOS 4.1 or later.
    NSString *reqSysVer = @"4.1";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    BOOL osVersionSupported = ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending);
    
    return (gcClass && osVersionSupported);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"MjehOO3b42eFjOeqO7tC6gqrg"
                                                            consumerSecret:@"eFqsEoE9otHPy0iL7pQZxTzbJx60ZhbDVUh889sqniLnjv1bNv"];
    
    [twitter verifyCredentialsWithSuccessBlock:^(NSString *bearerToken) {
     
        [twitter getSearchTweetsWithQuery:@"#PennyClicks" successBlock:^(NSDictionary *data, NSArray *tweets) {
               // NSLog(@"%@", tweets);
                //NSJSONSerialization *json = NSJSONSerialzation
                //NSArray *ar = NSJSONSerialization JSONObjectWith
            GameModel *gameModel = [GameModel sharedManager];
            [gameModel.tweets removeAllObjects];
            for (NSDictionary *tweet in tweets) {
                //NSLog(@"%@", tweet );
                NSMutableString *clean = [NSMutableString stringWithFormat:@"%@", [tweet objectForKey:@"text"]];
                [clean replaceOccurrencesOfString:@"fuck"
                                       withString:@"&#@!"
                                          options:NSCaseInsensitiveSearch
                                            range:NSMakeRange(0, [clean length])];
                [clean replaceOccurrencesOfString:@"shit"
                                       withString:@"„Ô3#"
                                          options:NSCaseInsensitiveSearch
                                            range:NSMakeRange(0, [clean length])];
                [clean replaceOccurrencesOfString:@"cock"
                                       withString:@"‚#Í¬"
                                          options:NSCaseInsensitiveSearch
                                            range:NSMakeRange(0, [clean length])];
                [clean replaceOccurrencesOfString:@"cunt"
                                       withString:@"´„€Ô"
                                          options:NSCaseInsensitiveSearch
                                            range:NSMakeRange(0, [clean length])];
                [clean replaceOccurrencesOfString:@"pussy"
                                       withString:@"˝Á§•"
                                          options:NSCaseInsensitiveSearch
                                            range:NSMakeRange(0, [clean length])];
                [clean replaceOccurrencesOfString:@"faggot"
                                       withString:@"˙ÁÍ#"
                                          options:NSCaseInsensitiveSearch
                                            range:NSMakeRange(0, [clean length])];
                [clean replaceOccurrencesOfString:@"nigger"
                                       withString:@"‹ÓÏO"
                                          options:NSCaseInsensitiveSearch
                                            range:NSMakeRange(0, [clean length])];
                [gameModel.tweets addObject:[NSString stringWithFormat:@"@%@: %@", [[tweet objectForKey:@"user"] objectForKey:@"screen_name" ], [tweet objectForKey:@"text"]]];
            }
            } errorBlock:^(NSError *error) {
                
                NSLog(@"%@", error.debugDescription);
                
            }];

    } errorBlock:^(NSError *error) {
        
        NSLog(@"%@", error.debugDescription);
        
    }];
    
    
    // enable game center
    self.gameCenterAuthenticationComplete = NO;
    
    if (!isGameCenterAPIAvailable()) {
        // Game Center is not available.
    } else {
        
        GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
        
        /*
         The authenticateWithCompletionHandler method is like all completion handler methods and runs a block
         of code after completing its task. The difference with this method is that it does not release the
         completion handler after calling it. Whenever your application returns to the foreground after
         running in the background, Game Kit re-authenticates the user and calls the retained completion
         handler. This means the authenticateWithCompletionHandler: method only needs to be called once each
         time your application is launched. This is the reason the sample authenticates in the application
         delegate's application:didFinishLaunchingWithOptions: method instead of in the view controller's
         viewDidLoad method.
         
         Remember this call returns immediately, before the user is authenticated. This is because it uses
         Grand Central Dispatch to call the block asynchronously once authentication completes.
         */
        [[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:^(NSError *error) {
            // If there is an error, do not assume local player is not authenticated.
            if (localPlayer.isAuthenticated) {
                
                // Enable Game Center Functionality
                self.gameCenterAuthenticationComplete = YES;
                
                if (! self.currentPlayerID || ! [self.currentPlayerID isEqualToString:localPlayer.playerID]) {
                    
                    // Switching Users
                    if (!self.player || ![self.currentPlayerID isEqualToString:localPlayer.playerID]) {
                        // If there is an existing player, replace the existing PlayerModel object with a
                        // new object, and use it to load the new player's saved achievements.
                        // It is not necessary for the previous PlayerModel object to writes its data first;
                        // It automatically saves the changes whenever its list of stored
                        // achievements changes.
                        
                        self.player = [[PlayerModel alloc] init];
                    }
                    [[self player] loadStoredScores];
                    [[self player] resubmitStoredScores];
                    
                    // Load new game instance around new player being logged in.
                    
                    
                }
                
            } else {
                NSLog(@"err:%@", error.description);
                // User has logged out of Game Center or can not login to Game Center, your app should run
				// without GameCenter support or user interface.
                self.gameCenterAuthenticationComplete = NO;
            }
        }];
    }

    return YES;
}


							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[GameModel sharedManager] saveGameState];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[GameModel sharedManager] saveGameState];
    
    
    // reprot to gamecenter
    if(self.gameCenterAuthenticationComplete) {
        GameModel *gameModel = [GameModel sharedManager];
        GKScore *scoreReporter = [[GKScore alloc] initWithLeaderboardIdentifier: @"r5k_pc_leaderboard"];
        scoreReporter.value = gameModel.lifetimePennies;
        scoreReporter.context = 0;

        NSArray *scores = @[scoreReporter];
        [GKScore reportScores:scores withCompletionHandler:nil];
        
    }

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"applicationWillTerminate");
    [[GameModel sharedManager] saveGameState];
}

@end
