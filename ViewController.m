//
//  ViewController.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "ViewController.h"
#import "MyScene.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
    
    // set up upgrades table view
    _upgradesTable = [[UpgradesViewController alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_upgradesTable];
    
    // set up statistics table view
    _statisticsTable = [[StatisticsViewController alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_statisticsTable];

    // set up bonus table view
    _bonusTable = [[BonusViewController alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_bonusTable];

    // Create and configure the scene.
    SKScene * scene = [MyScene sceneWithSize:skView.bounds.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    // Present the scene.
    [skView presentScene:scene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}


-(void)closeView
{
    
}

-(void) showUpgradesTable
{
    CGRect bounds = self.view.bounds;
    [UIView animateWithDuration:.25f
                     animations:^{
                         [_upgradesTable setFrame:CGRectMake(0, bounds.origin.y + 100, bounds.size.width, bounds.size.height - 100)];
                     }];

}

-(void) showStatisticsTable
{
    [self.statisticsTable.table reloadData];
    CGRect bounds = self.view.bounds;
    [UIView animateWithDuration:.25f
                     animations:^{
                         [_statisticsTable setFrame:CGRectMake(0, bounds.origin.y + 100, bounds.size.width, bounds.size.height - 100)];
                     }];
    
}

-(void) showBonusTable
{
    NSLog(@"hereqq");
    //[self.statisticsTable reloadData];
    CGRect bounds = self.view.bounds;
    [UIView animateWithDuration:.25f
                     animations:^{
                         [_bonusTable setFrame:CGRectMake(0, bounds.origin.y + 100, bounds.size.width, bounds.size.height - 100)];
                     }];
    
}

-(void) updateStatisticsTable
{
    //NSLog(@"Relaod %f, %f", _statisticsTable.frame.origin.y, self.view.bounds.size.height);
    if(_statisticsTable.frame.origin.y < self.view.bounds.size.height){
        [self.statisticsTable.table reloadData];
    }
}
@end
