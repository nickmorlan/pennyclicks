//
//  GameUpgrade.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/15/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameUpgrade : NSObject <NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSNumber *cost;
@property (nonatomic, strong) NSNumber *basePPS;
@property (nonatomic, strong) NSNumber *quantity;
@property (nonatomic, strong) NSNumber *penniesInvested;
@property (nonatomic, strong) NSNumber *penniesGenerated;
@property (nonatomic, strong) NSNumber *multiplyer;

-(NSString *)imageName;
-(void)powerUp;

@end
