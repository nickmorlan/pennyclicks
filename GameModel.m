//
//  GameModel.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/14/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "GameModel.h"

@implementation GameModel


#pragma mark Singleton Methods

+ (id)sharedManager {
    static GameModel *gameModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gameModel = [[self alloc] init];
    });
    return gameModel;
}

- (id)init {
    if (self = [super init]) {
        [self loadGameState];
    }

    return self;
}


#pragma mark -
#pragma mark Save/Load State
-(void) saveGameState
{
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *encoder = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [encoder encodeInt64:_totalClicks forKey:@"TotalClicks"];
    [encoder encodeInt64:_lifetimePennies forKey:@"LifetimePennies"];
    [encoder encodeInt64:_lifetimePenniesFromClicks forKey:@"LifetimePenniesFromClicks"];
    [encoder encodeInt64:_pennyBagValue forKey:@"PennyBagValue"];
    [encoder encodeInteger:_pennyBagDrops forKey:@"PennyBagDrops"];
    [encoder encodeInt64:_pennyBagLifetimeValue forKey:@"PennyBagLifetimeValue"];
    [encoder encodeInt64:_penniesSpent forKey:@"PenniesSpent"];
    [encoder encodeInt64:_pennyBank forKey:@"PennyBank"];
    [encoder encodeInt64:_bonusPennies forKey:@"BonusPennies"];
    [encoder encodeInteger:_numberOfTweets forKey:@"NumberOfTweets"];
    [encoder encodeInteger:_numberOfFacebookPosts forKey:@"NumberOfFacebookPosts"];
    [encoder encodeInteger:_dayOfLastTweet forKey:@"LastTweet"];
    [encoder encodeInteger:_dayOfLastFacebookPost forKey:@"LastFacebook"];
    [encoder encodeBool: _likedOnFacebook forKey:@"FacebookLike"];
    [encoder encodeBool: _doubleClicker forKey:@"DoubleClicker"];
    [encoder encodeFloat:_basePenniesPerSecond forKey:@"BasePenniesPerSecond"];
    [encoder encodeFloat:_totalPenniesPerSecond forKey:@"TotalPenniesPerSecond"];
    [encoder encodeInt:_clicksSinceLastUpdate forKey:@"ClicksSinceLastUpdate"];
    [encoder encodeInt:_upgradesPurchased forKey:@"UpgradesPurchased"];
    [encoder encodeObject:_clicksPerPastSeconds forKey:@"ClicksPerPastSeconds"];
    [encoder encodeObject:_gameUpgrades forKey:@"Upgrades"];
    [encoder encodeObject:_tweets forKey:@"Tweets"];
    [encoder finishEncoding];

    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"GameModel"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(void) loadGameState
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"GameModel"] != nil) {
        NSData *data = [defaults objectForKey:@"GameModel"];
        NSKeyedUnarchiver *decoder = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        _totalClicks = [decoder decodeInt64ForKey:@"TotalClicks"];
        _lifetimePennies = [decoder decodeInt64ForKey:@"LifeTimePennies"];
        _lifetimePenniesFromClicks = [decoder decodeInt64ForKey:@"LifetimePenniesFromClicks"];
        _pennyBagValue = [decoder decodeInt64ForKey:@"PennyBagValue"];
        _pennyBagDrops = [decoder decodeIntegerForKey:@"PennyBagDrops"];
        _pennyBagLifetimeValue = [decoder decodeInt64ForKey:@"PennyBagLifetimeValue"];
        _penniesSpent = [decoder decodeInt64ForKey:@"PenniesSpent"];
        _pennyBank = [decoder decodeInt64ForKey:@"PennyBank"];
        _bonusPennies = [decoder decodeInt64ForKey:@"BonusPennies"];
        _numberOfTweets = [decoder decodeIntegerForKey:@"NumberOfTweets"];
        _numberOfFacebookPosts = [decoder decodeIntegerForKey:@"NumberOfFacebookPosts"];
        _dayOfLastTweet = [decoder decodeIntegerForKey:@"LastTweet"];
        _dayOfLastFacebookPost = [decoder decodeIntegerForKey:@"LastFacebook"];
        _likedOnFacebook = [decoder decodeBoolForKey:@"FacebookLike"];
        _doubleClicker = [decoder decodeBoolForKey:@"DoubleClicker"];
        _basePenniesPerSecond = [decoder decodeFloatForKey:@"BasePenniesPerSecond"];
        _totalPenniesPerSecond = [decoder decodeFloatForKey:@"TotalPenniesPerSecond"];
        _clicksSinceLastUpdate = [decoder decodeIntForKey:@"ClicksSinceLastUpdate"];
        _upgradesPurchased = [decoder decodeIntForKey:@"UpgradesPurchased"];
        _clicksPerPastSeconds = [decoder decodeObjectForKey:@"ClicksPerPastSeconds"];
        _gameUpgrades = [decoder decodeObjectForKey:@"Upgrades"];
        _tweets = [decoder decodeObjectForKey:@"Tweets"];
    } else {
        _totalClicks = 0;
        _lifetimePennies = 0;
        _lifetimePenniesFromClicks = 0;
        _pennyBagValue = arc4random_uniform(100) + 100;
        _pennyBagDrops = 0;
        _pennyBagLifetimeValue = 0;
        _penniesSpent = 0;
        _bonusPennies = 0;
        _numberOfFacebookPosts = 0;
        _numberOfTweets = 0;
        _dayOfLastTweet = 0;
        _dayOfLastFacebookPost = 0;
        _likedOnFacebook = NO;
        _doubleClicker = NO;
        _pennyBank = 0;
        _basePenniesPerSecond = 0;
        _totalPenniesPerSecond = 0;
        _clicksPerPastSeconds = 0;
        _upgradesPurchased = 0;
        _clicksPerPastSeconds = [[NSMutableArray alloc] init];
        _tweets = [[NSMutableArray alloc] init];
        [self initializeGameUpgrades];
    }
    
   // NSLog(@"%llu", _pennyBagValue);

}


#pragma mark -
#pragma mark Shortcut Helpers

-(void) incrementPennyBank
{
    _pennyBank ++;
    _lifetimePennies ++;
    if (_doubleClicker) {
        _pennyBank ++;
        _lifetimePennies ++;
    }
}

-(void) addToPennyBank:(unsigned long long)pennies
{
    _pennyBank += pennies;
    _lifetimePennies += pennies;
}

-(void) addToLifetimePenniesFromClicks:(unsigned long long)pennies
{
    _lifetimePenniesFromClicks += pennies;
}

-(void) subtractFromPennyBank:(unsigned long long)pennies
{
    if (_pennyBank - pennies > 0) {
        _pennyBank -= pennies;
    }
}

-(void) incrementTotalClicks
{
    _totalClicks ++;
}

-(void) incrementClicksSinceLastUpdateBy:(int)value
{
    _clicksSinceLastUpdate += value;
}

-(void) incrementClicksSinceLastUpdate
{
    _clicksSinceLastUpdate ++;
}

-(void) resetClicksSinceLastUpdate
{
    _clicksSinceLastUpdate = 0;
}

-(void) calculateBasePenniesPerSecondBasedOnCurrentUpgrades
{
    float bpps = 0.;
    for (GameUpgrade * upgrade in _gameUpgrades) {
        if([upgrade.type isEqualToString:@"PPS"]) {
           // NSLog(@"%@ %@, %@, %f", upgrade.name, upgrade.basePPS, upgrade.quantity, [upgrade.basePPS floatValue] * [upgrade.quantity intValue]);
            bpps += [upgrade.basePPS floatValue] * [upgrade.quantity intValue];
        }
    }
    _basePenniesPerSecond = bpps;
    //NSLog(@"%f", bpps);
}

-(NSNumber *) getTweetBonusValue
{
    return [NSNumber numberWithInt:floor((self.numberOfTweets + 1) * (self.basePenniesPerSecond + 1) * 600)];
}

-(NSNumber *) getFacebookPostBonusValue
{
    return [NSNumber numberWithInt:floor((self.numberOfFacebookPosts + 2) * (self.basePenniesPerSecond + 1) * 600)];
}

-(BOOL) doUpgrade: (GameUpgrade *)upgrade
{
    if ([upgrade.quantity intValue] < 100) {
        if(_pennyBank > [upgrade.cost unsignedLongLongValue]){
            [self subtractFromPennyBank:[upgrade.cost unsignedLongLongValue]];
            NSLog(@"Upgrade %@ cost %llu", upgrade.name, [upgrade.cost unsignedLongLongValue]);
            [upgrade powerUp];
            [self calculateBasePenniesPerSecondBasedOnCurrentUpgrades];
            [self saveGameState];
            return YES;
        }
    }
    
    return NO;
}

-(void) setRandomClickModifier
{
    int rnd = arc4random_uniform(101);
    int value = 0;
    int multiplier = 1;
    if (_doubleClicker) {
        multiplier = 2;
    }
    if(rnd % 100 == 0) {
        value = 50 * multiplier;
    } else if(rnd % 10 == 0) {
        value = 20 * multiplier;
    } else if(rnd % 3 == 0) {
        value = 3 * multiplier;
    } else {
        value = 2 * multiplier;
    }
    [self setClickMultiplier:value];
}

-(NSNumber *) getPennyBagDropValue;
{
    NSNumber *value = [NSNumber numberWithLongLong:_pennyBagValue];
    _pennyBagDrops ++;
    _pennyBagLifetimeValue += _pennyBagValue;
    _pennyBagValue = floor(_pennyBagValue * 1.2);
    return value;
}

-(void) initializeGameUpgrades
{
    _gameUpgrades = [[NSMutableArray alloc] init];
    
    GameUpgrade *upgrade1 = [[GameUpgrade alloc] init];
    [upgrade1 setName:@"Auto Clicker"];
    [upgrade1 setDescription:@"Automatically clicks once every 10 seconds"];
    [upgrade1 setIcon:@"u0"];
    [upgrade1 setType:@"PPS"];
    [upgrade1 setCost:[NSNumber numberWithInt:30]];
    [upgrade1 setBasePPS:[NSNumber numberWithFloat:.1]];
    [upgrade1 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade1 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [upgrade1 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade1 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade1 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade1];
    
    
    GameUpgrade *upgrade2 = [[GameUpgrade alloc] init];
    [upgrade2 setName:@"Grandpa"];
    [upgrade2 setDescription:@"Have a penny kiddo"];
    [upgrade2 setIcon:@"u1"];
    [upgrade2 setType:@"PPS"];
    [upgrade2 setCost:[NSNumber numberWithInt:100]];
    [upgrade2 setBasePPS:[NSNumber numberWithFloat:.3]];
    [upgrade2 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade2 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade2 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade2 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade2];
    
    GameUpgrade *upgrade3 = [[GameUpgrade alloc] init];
    [upgrade3 setName:@"Coin Purse"];
    [upgrade3 setDescription:@"Have some spare change"];
    [upgrade3 setIcon:@"u2"];
    [upgrade3 setType:@"PPS"];
    [upgrade3 setCost:[NSNumber numberWithInt:1000]];
    [upgrade3 setBasePPS:[NSNumber numberWithFloat:1]];
    [upgrade3 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade3 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade3 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade3 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade3];
    
    GameUpgrade *upgrade4 = [[GameUpgrade alloc] init];
    [upgrade4 setName:@"Piggy Bank"];
    [upgrade4 setDescription:@"Ramp up penny production"];
    [upgrade4 setIcon:@"u3"];
    [upgrade4 setType:@"PPS"];
    [upgrade4 setCost:[NSNumber numberWithInt:10000]];
    [upgrade4 setBasePPS:[NSNumber numberWithFloat:3.1]];
    [upgrade4 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade4 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade4 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade4 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade4];

    GameUpgrade *upgrade5 = [[GameUpgrade alloc] init];
    [upgrade5 setName:@"Penny Bot"];
    [upgrade5 setDescription:@"Robo pennies"];
    [upgrade5 setIcon:@"u4"];
    [upgrade5 setType:@"PPS"];
    [upgrade5 setCost:[NSNumber numberWithInt:50000]];
    [upgrade5 setBasePPS:[NSNumber numberWithFloat:6]];
    [upgrade5 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade5 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade5 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade5 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade5];

    GameUpgrade *upgrade6 = [[GameUpgrade alloc] init];
    [upgrade6 setName:@"Penny Factory"];
    [upgrade6 setDescription:@"Working 9 to 5"];
    [upgrade6 setIcon:@"u5"];
    [upgrade6 setType:@"PPS"];
    [upgrade6 setCost:[NSNumber numberWithInt:200000]];
    [upgrade6 setBasePPS:[NSNumber numberWithFloat:20]];
    [upgrade6 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade6 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade6 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade6 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade6];

    GameUpgrade *upgrade7 = [[GameUpgrade alloc] init];
    [upgrade7 setName:@"Industrial Penny Parkway"];
    [upgrade7 setDescription:@"Automated mass penny production"];
    [upgrade7 setIcon:@"u6"];
    [upgrade7 setType:@"PPS"];
    [upgrade7 setCost:[NSNumber numberWithInt:500000]];
    [upgrade7 setBasePPS:[NSNumber numberWithFloat:40]];
    [upgrade7 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade7 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade7 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade7 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade7];

    GameUpgrade *upgrade8 = [[GameUpgrade alloc] init];
    [upgrade8 setName:@"Penny Cloner"];
    [upgrade8 setDescription:@"Carbon copied copper"];
    [upgrade8 setIcon:@"u7"];
    [upgrade8 setType:@"PPS"];
    [upgrade8 setCost:[NSNumber numberWithInt:100000]];
    [upgrade8 setBasePPS:[NSNumber numberWithFloat:70]];
    [upgrade8 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade8 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade8 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade8 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade8];
    
    GameUpgrade *upgrade9 = [[GameUpgrade alloc] init];
    [upgrade9 setName:@"Boss Penny Cloner"];
    [upgrade9 setDescription:@"Ultra copper carbon copies"];
    [upgrade9 setIcon:@"u8"];
    [upgrade9 setType:@"PPS"];
    [upgrade9 setCost:[NSNumber numberWithInt:5000000]];
    [upgrade9 setBasePPS:[NSNumber numberWithFloat:300]];
    [upgrade9 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade9 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade9 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade9 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade9];
    
    GameUpgrade *upgrade10 = [[GameUpgrade alloc] init];
    [upgrade10 setName:@"Penny Labratory"];
    [upgrade10 setDescription:@"Gettin scientific up in here"];
    [upgrade10 setIcon:@"u9"];
    [upgrade10 setType:@"PPS"];
    [upgrade10 setCost:[NSNumber numberWithInt:30000000]];
    [upgrade10 setBasePPS:[NSNumber numberWithFloat:1500]];
    [upgrade10 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade10 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade10 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade10 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade10];

    GameUpgrade *upgrade11 = [[GameUpgrade alloc] init];
    [upgrade11 setName:@"Labratory Lab Coat"];
    [upgrade11 setDescription:@"Who knew it produced pennies"];
    [upgrade11 setIcon:@"u10"];
    [upgrade11 setType:@"PPS"];
    [upgrade11 setCost:[NSNumber numberWithInt:70000000]];
    [upgrade11 setBasePPS:[NSNumber numberWithFloat:3000]];
    [upgrade11 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade11 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade11 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade11 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade11];

    GameUpgrade *upgrade12 = [[GameUpgrade alloc] init];
    [upgrade12 setName:@"Flux Capacitor"];
    [upgrade12 setDescription:@"Advaned tech ahead, be careful"];
    [upgrade12 setIcon:@"u11"];
    [upgrade12 setType:@"PPS"];
    [upgrade12 setCost:[NSNumber numberWithInt:200000000]];
    [upgrade12 setBasePPS:[NSNumber numberWithFloat:8000]];
    [upgrade12 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade12 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade12 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade12 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade12];

    GameUpgrade *upgrade13 = [[GameUpgrade alloc] init];
    [upgrade13 setName:@"Ancient Tech"];
    [upgrade13 setDescription:@"Ancient tech here, getting iffy"];
    [upgrade13 setIcon:@"u12"];
    [upgrade13 setType:@"PPS"];
    [upgrade13 setCost:[NSNumber numberWithInt:400000000]];
    [upgrade13 setBasePPS:[NSNumber numberWithFloat:15000]];
    [upgrade13 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade13 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade13 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade13 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade13];

    GameUpgrade *upgrade14 = [[GameUpgrade alloc] init];
    [upgrade14 setName:@"Penny Wishing Well"];
    [upgrade14 setDescription:@"Stealing childrens dreams"];
    [upgrade14 setIcon:@"u13"];
    [upgrade14 setType:@"PPS"];
    [upgrade14 setCost:[NSNumber numberWithInt:60000000]];
    [upgrade14 setBasePPS:[NSNumber numberWithFloat:20000]];
    [upgrade14 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade14 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade14 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade14 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade14];

    GameUpgrade *upgrade15 = [[GameUpgrade alloc] init];
    [upgrade15 setName:@"Quantum Penny Generator"];
    [upgrade15 setDescription:@"Tearing apart space time"];
    [upgrade15 setIcon:@"u14"];
    [upgrade15 setType:@"PPS"];
    [upgrade15 setCost:[NSNumber numberWithUnsignedLongLong:1000000000]];
    [upgrade15 setBasePPS:[NSNumber numberWithFloat:30000]];
    [upgrade15 setQuantity:[NSNumber numberWithInt:0]];
    [upgrade15 setPenniesInvested:[NSNumber numberWithInteger:0]];
    [upgrade15 setPenniesGenerated:[NSNumber numberWithInteger:0]];
    [upgrade15 setMultiplyer:[NSNumber numberWithFloat:0.]];
    [_gameUpgrades addObject:upgrade15];
}

-(void) updateUpgradeStats
{
    for (GameUpgrade *upgrade in _gameUpgrades) {
        if ([upgrade.quantity intValue] > 0) {
            upgrade.penniesGenerated = [NSNumber numberWithLong:([upgrade.penniesGenerated longLongValue] +floor([upgrade.quantity intValue] * [upgrade.basePPS floatValue]))];
        }
    }
}
@end
