//
//  UpgradesViewController.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/15/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAINLABEL_TAG 1
#define SECONDLABEL_TAG 2
#define PHOTO_TAG 3
#define QTYLABEL_TAG 4
#define COSTLABEL_TAG 5
#define PPSLABEL_TAG 6

@interface UpgradesViewController : UIView <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property UITableView *table;

@end
