//
//  InfoLayer.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "InfoLayer.h"
#import "GameModel.h"
@implementation InfoLayer
{
    SKLabelNode *_pennyBankLabel;
    SKLabelNode *_clicksPerSecondLabel;
    SKLabelNode *_scrollLabel;
    NSMutableArray *_scrollerTexts;
}

-(id) initForScene:(CGRect)scene
{
    if(self = [super initWithColor:[SKColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:.5] size:CGSizeMake(CGRectGetWidth(scene), 100)]) {
        self.anchorPoint = CGPointMake(0, 0);
        self.position = CGPointMake(0, CGRectGetHeight(scene) - 100);
        _pennyBankLabel = [SKLabelNode labelNodeWithFontNamed:@"AmericanTypewriter-CondensedBold"];
        _pennyBankLabel.position = CGPointMake((self.size.width / 2), (self.size.height / 2));
        _pennyBankLabel.fontSize = 28;
        _clicksPerSecondLabel = [SKLabelNode labelNodeWithFontNamed:@"AmericanTypewriter-CondensedBold"];
        _clicksPerSecondLabel.position = CGPointMake((self.size.width / 2), (self.size.height / 4));
        _clicksPerSecondLabel.fontSize = 14;
        _scrollLabel = [SKLabelNode labelNodeWithFontNamed:@"AmericanTypewriter-CondensedBold"];
        _scrollLabel.position = CGPointMake(self.size.width + 5, self.size.height - 15);
        _scrollLabel.fontSize = 12;
        _scrollLabel.fontColor = [UIColor yellowColor];
        [self addChild:_pennyBankLabel];
        [self addChild:_clicksPerSecondLabel];
        [self addChild:_scrollLabel];
        //[self startLabelScrollerWithText:@"asf"];
        _scrollerTexts = [[NSMutableArray alloc] init];
        GameModel *gameModel = [GameModel sharedManager];
        if(!gameModel.tweets) {
            [_scrollerTexts addObject:[[NSMutableArray alloc] init]];
        } else {
            [_scrollerTexts addObject:gameModel.tweets];
        }
        [_scrollerTexts addObject:@[@"A new PennyClick Player has been found.",
                                    @"Small change is a big deal.",
                                    @"The news has reported a bigfoot sighting.",
                                    @"There are 100 pennies in a dollar.",
                                    @"If you drop a penny from the Empire State Building you will lose money.",
                                    @"A doubleclicker purchase can be restored if you erase the game.",
                                    @"∆∆∆∆ #PennyClicks!!"]];
        [_scrollerTexts addObject:@[@"Penny production has ramped up.",
                                    @"Grandpas sighted throwing pennies at grandchildren.",
                                    @"How many pennies does it take to fill in a football stadium?",
                                    @"Some pennies were found in the Hudson River.",
                                    @"Are pennies still made with copper?",
                                    @"∆∆∆∆ #PennyClicks!!"]];
        [_scrollerTexts addObject:@[@"There are pennies falling from othe sky.",
                                    @"Pennies are cooler than dollars.",
                                    @"Pick up all the pennies you find on the ground.",
                                    @"An all penny diet is considered vegan.",
                                    @"It only takes 384,403,000,000 pennies stacked on each other to reach the moon.",
                                    @"¢¶¨¶¨˜ø∆¬∆∂ª™øˆ˙˜˜˜ƒ∂ßß•™ ©ˆƒ¨¥ƒª¶¥•¶™¶©•§ø°‡˝ﬂ‡°ıÓÔÍı˜Í◊˝◊¨ÅÍÎ∏·ˆ‡"]];
        [_scrollerTexts addObject:@[@"NASA goes to mars in search of copper mines.",
                                    @"Local wishing wells have been cleaned out of all thier pennies.. only quarters and nickels remain.",
                                    @"Why are you wasting your time?",
                                    @"You will never make it to the end.",
                                    @"One old man believes that pennies are made in the kidneys.",
                                    @"In two years time there will be no pennies left on earth."]];
        [_scrollerTexts addObject:@[@"All known power wires stripped bare in a mad rush to mine copper.",
                                    @"Red clouds are on the horizon.",
                                    @"Dont ever thow pennies at someone unless you want to lose money.",
                                    @"Scientist invent a car with zero emissions, but it does not make ice cream.",
                                    @"Reports of local man turning into bronze statue after staring at his penny collection are 100% true.",
                                    @"If you tear a dollar bill into 100 pieces you will not have 100 pennies."]];
        [_scrollerTexts addObject:@[@"New 50 cent piece rejected by government lobbiests.",
                                    @"One baseball player calls PennyClicks 'Rediculous Garbage'. He was later hit with a pitch.",
                                    @"Local pig farm looted, the theives were looking for pennies.",
                                    @"Researchers looking into antimatter vortexes to create more pennies.",
                                    @"Kittens have become extinct.",
                                    @"Tweet #PennyClicks to show up on the ticker."]];
       
        
    }
    return self;
}

-(void) initializeLayerSettings:(unsigned long long)bank
{
    //self.anchorPoint = CGPointMake(0, 0);
    //self.position = CGPointMake(0, CGRectGetHeight(self.parent.frame) - 100);
    [self setPennyBankTo:bank];
}

-(void) setPennyBankTo:(unsigned long long)bank
{
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    _pennyBankLabel.text = [NSString stringWithFormat:@"%@ Pennies", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:bank]]];
}

-(void) setPenniesPerSecondTo:(float)pps
{
    if (pps > 1000) {
        pps = floorf(pps);
    }
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    _clicksPerSecondLabel.text = [NSString stringWithFormat:@"%@ per second", [format stringFromNumber:[NSNumber numberWithFloat:pps]]];

}

-(void) setupNewScrollerTextFromPPS:(float)pps
{
    int chance = arc4random_uniform(10);
    NSLog(@"%d", chance);
    if(([[_scrollerTexts objectAtIndex:0] count] > 0) & (chance % 2 == 0)) {
        NSArray *textList = [_scrollerTexts objectAtIndex:0];
        int rnd = arc4random_uniform([textList count]);
        //NSLog(@"%@", _scrollerTexts);
        [self startLabelScrollerWithText:[textList objectAtIndex:rnd]];
    } else {
        if (pps < 10) {
            NSArray *textList = [_scrollerTexts objectAtIndex:1];
            int rnd = arc4random_uniform([textList count]);
            //NSLog(@"%@", _scrollerTexts);
            [self startLabelScrollerWithText:[textList objectAtIndex:rnd]];
        } else if (pps < 25) {
            NSArray *textList = [_scrollerTexts objectAtIndex:2];
            int rnd = arc4random_uniform([textList count]);
            //NSLog(@"%@", textList);
            [self startLabelScrollerWithText:[textList objectAtIndex:rnd]];
        } else if (pps < 100) {
            NSArray *textList = [_scrollerTexts objectAtIndex:2];
            int rnd = arc4random_uniform([textList count]);
            //NSLog(@"%@", textList);
            [self startLabelScrollerWithText:[textList objectAtIndex:rnd]];
        } else if (pps < 250) {
            NSArray *textList = [_scrollerTexts objectAtIndex:2];
            int rnd = arc4random_uniform([textList count]);
            //NSLog(@"%@", textList);
            [self startLabelScrollerWithText:[textList objectAtIndex:rnd]];
        } else if (pps < 1000) {
            int rnd1 = arc4random_uniform([_scrollerTexts count] - 1);
            NSArray *textList = [_scrollerTexts objectAtIndex:rnd1];
            int rnd = arc4random_uniform([textList count]);
            //NSLog(@"%@", textList);
            [self startLabelScrollerWithText:[textList objectAtIndex:rnd]];
        } else {
            int rnd1 = arc4random_uniform([_scrollerTexts count]);
            NSLog(@"%d", rnd1);
            NSArray *textList = [_scrollerTexts objectAtIndex:rnd1];
            int rnd = arc4random_uniform([textList count]);
            //NSLog(@"%@", textList);
            [self startLabelScrollerWithText:[textList objectAtIndex:rnd]];
        }
    }
}

-(void) startLabelScrollerWithText:(NSString *) text
{
   // NSLog(@"%f", self.parent.frame.size.width + _scrollLabel.frame.size.width);
    _scrollLabel.text = text;
    _scrollLabel.position = CGPointMake(self.size.width + _scrollLabel.frame.size.width, self.size.height - 15);
    SKAction *action = [SKAction moveByX:-(self.size.width + (_scrollLabel.frame.size.width * 2)) y:0 duration:10];
    [_scrollLabel runAction:action];
}
@end
