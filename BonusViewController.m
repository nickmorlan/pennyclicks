//
//  BonusViewController.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/21/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "BonusViewController.h"
#import "GameModel.h"
#import "PCIAPHelper.h"

@implementation BonusViewController
{
    GameModel *_gameModel;
    SKProduct *product;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    // Configure the view.
    //SKView * skView = (SKView *)self.view;
    //skView.showsFPS = YES;
    //skView.showsNodeCount = YES;
    
    CGRect bounds = self.bounds;
    [self setBackgroundColor:[UIColor whiteColor]];
    [self setFrame:CGRectMake(0,  bounds.size.height, bounds.size.width, bounds.size.height - 100)];

    // set up top border
    UIImage *bg = [[UIImage imageNamed:@"bordertop"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
    [border setBackgroundColor:[UIColor colorWithPatternImage:bg]];
    [self addSubview:border];
    
    // set up upgrades table view
    _table = [[UITableView alloc] initWithFrame:frame];
    _table.delegate = self;
    _table.dataSource = self;
    _table.separatorColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.2];
    _table.separatorColor = [UIColor clearColor];
    _table.pagingEnabled = YES;
    _table.allowsMultipleSelection = NO;
    [_table setSeparatorInset:UIEdgeInsetsMake(15, 15, 15, 15)];

    int adView = (UI_USER_INTERFACE_IDIOM () == UIUserInterfaceIdiomPad) ? 66 : 50;
    [_table setFrame:CGRectMake(0,  _table.frame.origin.y + 40, bounds.size.width, bounds.size.height - 140 - adView)];
    [self addSubview:_table];
    
     UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(bounds.size.width - 50, 5.0, 30.0, 30.0)];
    [button setBackgroundImage:[UIImage imageNamed:@"buttonclose"] forState:UIControlStateNormal];
    button.hidden = NO;
    [button setBackgroundColor:[UIColor clearColor]];
    [button addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchDown];
    [self addSubview:button];

    // initialize game model
    _gameModel = [GameModel sharedManager];

    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 7;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier0 = @"Cell0";
    UITableViewCell *cell;
    
    
        //GameUpgrade *upgrade = [_gameModel.gameUpgrades objectAtIndex:indexPath.row];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            cell.tag = indexPath.row;
            [self configureTableCellRows:cell];
        } else {
            cell = [self.table dequeueReusableCellWithIdentifier:CellIdentifier0];
           // [self updateTableCellRows:cell upgrade:upgrade];
            
        }
        
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 75;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(void) configureTableCellRows:(UITableViewCell *)cell
{
    UILabel *mainLabel, *detailTextLabel, *ppsLabel;
    UIImageView *photo;
    
    //NSLog(@"draw %@", upgrade.name);
    mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(63.0, 3.0, 175.0, 20.0)];
    mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    mainLabel.textColor = [UIColor blackColor];
    mainLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    
    detailTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(63.0, 10.0, 260.0, 60.0)];
    detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-light" size:12];
    detailTextLabel.textColor = [UIColor blackColor];
    detailTextLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    detailTextLabel.numberOfLines = 0;
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    
    ppsLabel = [[UILabel alloc] initWithFrame:CGRectMake(63.0, 50.0, 220.0, 20.0)];
    ppsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    ppsLabel.textColor = [UIColor redColor];
    ppsLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    
    photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 45.0)];
    photo.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    photo.contentMode = UIViewContentModeScaleAspectFit;

    if(cell.tag == 0) {
        //NSLog(@"draw %@", upgrade.name);
        mainLabel.text = @"Share on Facebook";
        detailTextLabel.text = @"Share us on facebook and receive package of bonus pennies";
        ppsLabel.text = [NSString stringWithFormat:@"+%d pennies", [[_gameModel getFacebookPostBonusValue] intValue]];
        photo.image = [UIImage imageNamed:@"facebookicon"];
        if([self canFBPostToday] == NO) {
            UIView *overlay = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, self.frame.size.width, 75)];
            [overlay setBackgroundColor:[UIColor colorWithWhite:1 alpha:.75]];
            [cell addSubview:overlay];
        }
    } else if (cell.tag == 1) {
        //NSLog(@"draw %@", upgrade.name);
        mainLabel.text = @"Tweet #PennyClicks";
        detailTextLabel.text = @"Tweet the hashtag once every day to recieve bonus pennies";
        ppsLabel.text = [NSString stringWithFormat:@"+%d pennies", [[_gameModel getTweetBonusValue] intValue]];
        photo.image = [UIImage imageNamed:@"twittericon"];
        if([self canTweetToday] == NO) {
            UIView *overlay = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, self.frame.size.width, 75)];
            [overlay setBackgroundColor:[UIColor colorWithWhite:1 alpha:.75]];
            [cell addSubview:overlay];
        }
    } else if (cell.tag == 2) {
        mainLabel.text = @"Purchase Double Clicker";
        mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        detailTextLabel.text = @"Unlock the double clicker and every click is worth double the pennies";
        ppsLabel.text = [NSString stringWithFormat:@"$.99"];
        ppsLabel.textColor = [UIColor greenColor];
        photo.image = [UIImage imageNamed:@"doubleclicksicon"];
    
        if(_gameModel.doubleClicker == YES) {
            UIView *overlay = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, self.frame.size.width, 75)];
            [overlay setBackgroundColor:[UIColor colorWithWhite:1 alpha:.75]];
            [cell addSubview:overlay];
        }
        
    } else if (cell.tag == 3) {
        mainLabel.text = @"Purchase Piggy Bank";
        mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        detailTextLabel.text = @"Purchase a piggy bank bag with 1,507,669,098 pennies.";
        ppsLabel.text = [NSString stringWithFormat:@"$.99"];
        ppsLabel.textColor = [UIColor greenColor];
        photo.image = [UIImage imageNamed:@"piggybankicon"];
    }
    else if (cell.tag == 4) {
        mainLabel.text = @"Purchase Money Bag";
        mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        detailTextLabel.text = @"Purchase a money bag filled with 1,899,332,573,098  pennies.";
        ppsLabel.text = [NSString stringWithFormat:@"$1.99"];
        ppsLabel.textColor = [UIColor greenColor];
        photo.image = [UIImage imageNamed:@"moneybagicon"];
    }
    else if (cell.tag == 5) {
        mainLabel.text = @"Restore Double Clicker";
        mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        detailTextLabel.text = @"Restore a previously purchased Double Clicker.";
        ppsLabel.text = [NSString stringWithFormat:@""];
        ppsLabel.textColor = [UIColor greenColor];
        photo.image = [UIImage imageNamed:@"doubleclicksicon"];
        if(_gameModel.doubleClicker == YES) {
            UIView *overlay = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y, self.frame.size.width, 75)];
            [overlay setBackgroundColor:[UIColor colorWithWhite:1 alpha:.75]];
            [cell addSubview:overlay];
        }
        
    }
    [cell.contentView addSubview:mainLabel];
    [cell.contentView addSubview:detailTextLabel];
    [cell.contentView addSubview:ppsLabel];
    [cell.contentView addSubview:photo];

}

-(void) doTweet
{

        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
            UIViewController *controller = (UIViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet setInitialText:@"#PennyClicks rocks!"];
            tweetSheet.completionHandler =  ^(SLComposeViewControllerResult result) {
                switch(result) {
                        //  This means the user cancelled without sending the Tweet
                    case SLComposeViewControllerResultCancelled:
                        break;
                        //  This means the user hit 'Send'
                    case SLComposeViewControllerResultDone: {
                        NSDate *now = [NSDate date];
                        NSCalendar *gregorian = [NSCalendar currentCalendar];
                        NSDateComponents *dateComponents = [gregorian components:(NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit) fromDate:now];
                        NSInteger day = [dateComponents day];
                        unsigned long long value = [[_gameModel getTweetBonusValue] unsignedLongLongValue];
                        _gameModel.dayOfLastTweet = day;
                        _gameModel.bonusPennies += value;
                        [_gameModel addToPennyBank:value];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"playCashRegister" object:nil];
                        [self.table reloadData];
                    }
                        break;
                }
            };
            
            [controller presentViewController:tweetSheet animated:YES completion:nil];
        } else {
            UIAlertView *alertView = [[UIAlertView alloc]
                                      initWithTitle:@"Sorry"
                                      message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
            [alertView show];
        }

}

-(void) doFacebook
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        UIViewController *controller = (UIViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
        SLComposeViewController *fb = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeFacebook];
        [fb setInitialText:@"I just got FREE pennies for #PennyClicks .. yeah!"];
        fb.completionHandler =  ^(SLComposeViewControllerResult result) {
            switch(result) {
                    //  This means the user cancelled without sending the Tweet
                case SLComposeViewControllerResultCancelled:
                    break;
                    //  This means the user hit 'Send'
                case SLComposeViewControllerResultDone: {
                    NSDate *now = [NSDate date];
                    NSCalendar *gregorian = [NSCalendar currentCalendar];
                    NSDateComponents *dateComponents = [gregorian components:(NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit) fromDate:now];
                    NSInteger day = [dateComponents day];
                    _gameModel.dayOfLastFacebookPost = day;
                    unsigned long long value = [[_gameModel getTweetBonusValue] unsignedLongLongValue];
                    _gameModel.bonusPennies += value;
                    [_gameModel addToPennyBank:value];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"playCashRegister" object:nil];
                   [self.table reloadData];
                }
                    break;
            }
        };
        
        [controller presentViewController:fb animated:YES completion:nil];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't post to Facebook right now, make sure your device has an internet connection and you have a Facebook account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
 
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if ([self canFBPostToday]) {
            [self doFacebook];
        }
    } else if (indexPath.row == 1) {
        if ([self canTweetToday]) {
            [self doTweet];
        }
    }else if (indexPath.row == 2) {
        if(_gameModel.doubleClicker == NO) {
            [self purchaseDoubleClick];
        }
    }else if (indexPath.row == 3) {
        [self purchasePiggyBank];
    }else if (indexPath.row == 4) {
        [self purchaseMoneyBag];
    }else if (indexPath.row == 5) {
        [[PCIAPHelper sharedInstance] restoreCompletedTransactions];
        //[[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }

    
}

-(BOOL) canTweetToday
{
    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [gregorian components:(NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit) fromDate:now];
    NSInteger day = [dateComponents day];
    
    if (_gameModel.dayOfLastTweet < day) {
        return YES;
    }
    if ((_gameModel.dayOfLastTweet > 28) & (day < _gameModel.dayOfLastTweet)) {
        return YES;
    }
    
    return NO;
}

-(BOOL) canFBPostToday
{
    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [gregorian components:(NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit) fromDate:now];
    NSInteger day = [dateComponents day];
    
    if (_gameModel.dayOfLastFacebookPost < day) {
        return YES;
    }
    if ((_gameModel.dayOfLastFacebookPost > 28) & (day < _gameModel.dayOfLastFacebookPost)) {
        return YES;
    }
    
    return NO;
}

-(void) closeView
{
    CGRect bounds = self.bounds;
    [UIView animateWithDuration:.15f
                     animations:^{
                         [self setFrame:CGRectMake(0,  bounds.size.height + 100, bounds.size.width, bounds.size.height - 100)];
                     }];
    
}


#pragma -
#pragma Purchases
- (void) purchaseDoubleClick
{
    [[PCIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if(success) {
            NSLog(@"%@", products);
            product = [products objectAtIndex:0];
            UIAlertView *tmp = [[UIAlertView alloc]
                                initWithTitle:@"Proceed with Purchase"
                                message:[NSString stringWithFormat:@"Do you want to purchase the lifetime Double Click upgrade for %@? This will multiply every click value by 2.", product.price]
                                delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:@"Yes", @"No", nil];
            tmp.tag = 1;
            [tmp show];
            //[[PCIAPHelper sharedInstance] buyProduct:[products objectAtIndex:0]];
        } else {
            UIAlertView *tmp = [[UIAlertView alloc]
                                initWithTitle:@"Not Available"
                                message:@"No products to purchase"
                                delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:@"Ok", nil];
            [tmp show];
        }
    }];
    
    
}

- (void) purchasePiggyBank
{
    [[PCIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if(success) {
            NSLog(@"%@", products);
            product = [products objectAtIndex:2];
            UIAlertView *tmp = [[UIAlertView alloc]
                                initWithTitle:@"Proceed with Purchase"
                                message:[NSString stringWithFormat:@"Do you want to purchase the Piggy Bank for %@? This will give you 1,507,669,098 pennies.", product.price]
                                delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:@"Yes", @"No", nil];
            tmp.tag = 2;
            [tmp show];
            //[[PCIAPHelper sharedInstance] buyProduct:[products objectAtIndex:0]];
        } else {
            UIAlertView *tmp = [[UIAlertView alloc]
                                initWithTitle:@"Not Available"
                                message:@"No products to purchase"
                                delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:@"Ok", nil];
            [tmp show];
        }
    }];
    
    
}

- (void) purchaseMoneyBag
{
    [[PCIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if(success) {
            NSLog(@"%@", products);
            product = [products objectAtIndex:1];
            UIAlertView *tmp = [[UIAlertView alloc]
                                initWithTitle:@"Proceed with Purchase"
                                message:[NSString stringWithFormat:@"Do you want to purchase the Money Bag for %@? This will give you 1,899,332,573,098 pennies.", product.price]
                                delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:@"Yes", @"No", nil];
            tmp.tag = 2;
            [tmp show];
            //[[PCIAPHelper sharedInstance] buyProduct:[products objectAtIndex:0]];
        } else {
            UIAlertView *tmp = [[UIAlertView alloc]
                                initWithTitle:@"Not Available"
                                message:@"No products to purchase"
                                delegate:self
                                cancelButtonTitle:nil
                                otherButtonTitles:@"Ok", nil];
            [tmp show];
        }
    }];
    
    
}


- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // the user clicked one of the OK/Cancel buttons
    // tag 1 is the double clicker
    if(actionSheet.tag == 1) {
        if (buttonIndex == 0) {
            [[PCIAPHelper sharedInstance] buyProduct:product];
        }
    } else if (actionSheet.tag == 2){
        if (buttonIndex == 0) {
            [[PCIAPHelper sharedInstance] buyProduct:product];
        }   
    } else if(actionSheet.tag == 77) {
        if (buttonIndex == 0)
        {
            NSString *reviewURL = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=853940850";
            
            // iOS 7 needs a different templateReviewURL @see https://github.com/arashpayan/appirater/issues/131
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
                reviewURL = @"itms-apps://itunes.apple.com/app/id853940850";
            }
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:reviewURL]];
        }
        else if(buttonIndex == 2)
        {
            [[NSUserDefaults standardUserDefaults] setInteger: 1 forKey:@"DoNotAskToRate"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
    }
}


@end
