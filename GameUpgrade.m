//
//  GameUpgrade.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/15/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "GameUpgrade.h"

@implementation GameUpgrade


- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if(self) {
        self.name = [decoder decodeObjectForKey:@"Name"];
        self.description = [decoder decodeObjectForKey:@"Description"];
        self.icon = [decoder decodeObjectForKey:@"Icon"];
        self.type = [decoder decodeObjectForKey:@"Type"];
        self.cost = [decoder decodeObjectForKey:@"Cost"];
        self.basePPS = [decoder decodeObjectForKey:@"BasePPS"];
        self.quantity = [decoder decodeObjectForKey:@"Quantity"];
        self.penniesInvested = [decoder decodeObjectForKey:@"Invested"];
        self.penniesGenerated = [decoder decodeObjectForKey:@"Generated"];
        self.multiplyer = [decoder decodeObjectForKey:@"Multiplyer"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.name forKey:@"Name"];
    [encoder encodeObject:self.description forKey:@"Description"];
    [encoder encodeObject:self.icon forKey:@"Icon"];
    [encoder encodeObject:self.type forKey:@"Type"];
    [encoder encodeObject:self.cost forKey:@"Cost"];
    [encoder encodeObject:self.basePPS forKey:@"BasePPS"];
    [encoder encodeObject:self.quantity forKey:@"Quantity"];
    [encoder encodeObject:self.penniesInvested forKey:@"Invested"];
    [encoder encodeObject:self.penniesGenerated forKey:@"Generated"];
    [encoder encodeObject:self.multiplyer forKey:@"Multiplyer"];
}


+ (NSDictionary *)imageMap {
    // 1
    static NSDictionary *_imageMap = nil;
    if (! _imageMap) {
        // 2
        _imageMap = @{
                      @"Auto Clicker" : @"u0",
                      @"Grandpa" : @"u1",
                      @"Coin Purse" : @"u2",
                      @"Piggy Bank" : @"u3",
                      @"Penny Bot" : @"u4",
                      @"Penny Factory" : @"u5",
                      @"Industrial Penny Parkway" : @"u6",
                      @"Penny Cloner" : @"u7",
                      @"Boss Penny Cloner" : @"u8",
                      @"Penny Labratory" : @"u9",
                      @"Labratory Lab Coat" : @"u10",
                      @"Flux Capacitor" : @"u11",
                      @"Ancient Tech" : @"u12",
                      @"Penny Wishing Well" : @"u13",
                      @"Quantum Penny Generator" : @"u14",
                      };
    }
    return _imageMap;
}

// 3
- (NSString *)imageName {
    return [GameUpgrade imageMap][self.icon];
}

-(void)powerUp
{
    if([_quantity intValue] < 100) {
        _quantity = [NSNumber numberWithInt:[_quantity intValue] + 1];
        _penniesInvested = [NSNumber numberWithUnsignedLongLong:[_penniesInvested unsignedLongLongValue] + [_cost intValue]];
        _cost = [NSNumber numberWithUnsignedLongLong:[_cost unsignedLongLongValue] * 1.3];
    }

}


@end
