//
//  GameModel.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/14/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameUpgrade.h"

@interface GameModel : NSObject <NSCoding>
// lifetime total clicks
@property (nonatomic) unsigned long long totalClicks;
// lifetime total pennies collected
@property (nonatomic) unsigned long long lifetimePennies;
// lifetime total pennies collected from clicking
@property (nonatomic) unsigned long long lifetimePenniesFromClicks;
// lifetime total pennies spent on upgrades
@property (nonatomic) unsigned long long penniesSpent;
// current penny bank amount
@property (nonatomic) unsigned long long pennyBank;
// lifetime bonus pennies collected
@property (nonatomic) unsigned long long bonusPennies;
// value for the random bag drops
@property (nonatomic) unsigned long long pennyBagValue;
// value for the random bag drops
@property (nonatomic) int pennyBagDrops;
// value for the random bag drops
@property (nonatomic) unsigned long long pennyBagLifetimeValue;
// currently in bonus time
@property (nonatomic) BOOL inBonus;
// user purchased double clicker
@property (nonatomic) BOOL doubleClicker;
// multiplier for bonus pennies during bonus time
@property (nonatomic) int clickMultiplier;
// number of tweets sent for bonus pennies
@property (nonatomic) int numberOfTweets;
// number of facebook posts for bonus pennies
@property (nonatomic) int numberOfFacebookPosts;
// day of month of last tweet
@property (nonatomic) int dayOfLastTweet;
// day of month of last fb post
@property (nonatomic) int dayOfLastFacebookPost;
// whether or not user liked fb page
@property (nonatomic) BOOL likedOnFacebook;
// base pennies per second
@property (nonatomic) float basePenniesPerSecond;
// average current pennies per second including clicking
@property (nonatomic) float totalPenniesPerSecond;
// number of upgrades
@property (nonatomic) int upgradesPurchased;
// click counter for calculations
@property (nonatomic) int clicksSinceLastUpdate;
// array of click counts to calculate average pps
@property (nonatomic) NSMutableArray *clicksPerPastSeconds;
// upgrade objects
@property (nonatomic) NSMutableArray *gameUpgrades;
// to save gamemodel to nsuserdefaults
@property (nonatomic) NSCoder *encoder;
// to hold tweets pulled in from web api
@property (nonatomic) NSMutableArray *tweets;

+(id) sharedManager;
-(void) saveGameState;
-(void) incrementPennyBank;
-(void) addToPennyBank:(unsigned long long)pennies;
-(void) addToLifetimePenniesFromClicks:(unsigned long long)pennies;
-(void) incrementTotalClicks;
-(void) incrementClicksSinceLastUpdate;
-(void) incrementClicksSinceLastUpdateBy:(int)value;
-(void) resetClicksSinceLastUpdate;
-(BOOL) doUpgrade: (GameUpgrade *)upgrade;
-(void) updateUpgradeStats;
-(void) setRandomClickModifier;
-(NSNumber *) getTweetBonusValue;
-(NSNumber *) getFacebookPostBonusValue;
-(NSNumber *) getPennyBagDropValue;

@end

