//
//  InfoLayer.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface InfoLayer : SKSpriteNode

-(id) initForScene:(CGRect)scene;
-(void) initializeLayerSettings:(unsigned long long )bank;
-(void) setPennyBankTo:(unsigned long long)bank;
-(void) setPenniesPerSecondTo:(float)pps;
-(void) setupNewScrollerTextFromPPS:(float)pps;

@end
