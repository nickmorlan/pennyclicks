//
//  Penny.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "Penny.h"

@implementation Penny


-(id) init {
    SKTextureAtlas *assets = [SKTextureAtlas atlasNamed:@"assets"];
    if (self = [super initWithTexture:[assets textureNamed:@"penny"]]) {
        [self setScale:.6];
    } else {
        NSLog(@"Error initializing Penny");
        return nil;
    }
    return self;

}

-(id) initWithDoubleClicker {
    SKTextureAtlas *assets = [SKTextureAtlas atlasNamed:@"assets"];
    if (self = [super initWithTexture:[assets textureNamed:@"doublepenny"]]) {
        [self setScale:.6];
    } else {
        NSLog(@"Error initializing Penny");
        return nil;
    }
    return self;
    
}
-(BOOL) isTouched
{
    return self.touched;
}
@end
