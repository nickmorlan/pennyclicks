//
//  AppDelegate.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerModel.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


// currentPlayerID is the value of the playerID last time we authenticated.
@property (retain,readwrite) NSString * currentPlayerID;

@property (nonatomic, retain) PlayerModel *player;

// isGameCenterAuthenticationComplete is set after authentication, and authenticateWithCompletionHandler's completionHandler block has been run. It is unset when the application is backgrounded.
@property (readwrite, getter=isGameCenterAuthenticationComplete) BOOL gameCenterAuthenticationComplete;

@end
