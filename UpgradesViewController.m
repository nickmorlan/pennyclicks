//
//  UpgradesViewController.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/15/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "UpgradesViewController.h"
#import "GameModel.h"
#import "GameUpgrade.h"

@interface UpgradesViewController ()
{
    GameModel *_gameModel;
}
@end

@implementation UpgradesViewController

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    CGRect bounds = self.bounds;
    [self setBackgroundColor:[UIColor clearColor]];
    [self setFrame:CGRectMake(0,  bounds.size.height, bounds.size.width, bounds.size.height - 100)];
    
    // set up top border
    UIImage *bg = [[UIImage imageNamed:@"bordertop"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
    [border setBackgroundColor:[UIColor colorWithPatternImage:bg]];
    [self addSubview:border];
    
    // set up upgrades table view
    _table = [[UITableView alloc] initWithFrame:frame];
    _table.delegate = self;
    _table.dataSource = self;
    _table.separatorColor = [UIColor colorWithWhite:1 alpha:.2];
    _table.pagingEnabled = YES;
    _table.allowsMultipleSelection = NO;
    
    int adView = (UI_USER_INTERFACE_IDIOM () == UIUserInterfaceIdiomPad) ? 66 : 50;
    [_table setFrame:CGRectMake(0,  _table.frame.origin.y + 40, bounds.size.width, bounds.size.height - 140 - adView)];
    [self addSubview:_table];
    
    // close button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(bounds.size.width - 50, 5.0, 30.0, 30.0)];
    [button setBackgroundImage:[UIImage imageNamed:@"buttonclose"] forState:UIControlStateNormal];
    button.hidden = NO;
    [button setBackgroundColor:[UIColor clearColor]];
    [button addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchDown];
    [self addSubview:button];
    
    // initialize GameModel instance
    _gameModel = [GameModel sharedManager];

    return self;
}


#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return [_gameModel.gameUpgrades count];
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier0 = @"Cell0";
    static NSString *CellIdentifier1 = @"Cell1";
    UITableViewCell *cell;
    
    
    if(indexPath.section == 0) {
        GameUpgrade *upgrade = [_gameModel.gameUpgrades objectAtIndex:indexPath.row];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
            cell.textLabel.textColor = [UIColor whiteColor];
            [self configureTableCellRows:cell upgrade:upgrade];
        } else {
            cell = [self.table dequeueReusableCellWithIdentifier:CellIdentifier0];
            [self updateTableCellRows:cell upgrade:upgrade];
            
        }
        
    } else if (indexPath.section == 1) {
        GameUpgrade *upgrade = [_gameModel.gameUpgrades objectAtIndex:indexPath.row];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
            cell.textLabel.textColor = [UIColor whiteColor];
            [self configureTableCellRows:cell upgrade:upgrade];
        } else {
            cell = [self.table dequeueReusableCellWithIdentifier:CellIdentifier1];
            [self updateTableCellRows:cell upgrade:upgrade];
        }
    }
    return cell;

    

    
}



#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

-(void) configureTableCellRows:(UITableViewCell *)cell upgrade:(GameUpgrade *)upgrade
{
    UILabel *mainLabel, *detailTextLabel, *qtyLabel, *costLabel, *ppsLabel;
    UIImageView *photo;
    
    //NSLog(@"draw %@", upgrade.name);
    mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 3.0, 175.0, 15.0)];
    mainLabel.tag = MAINLABEL_TAG;
    mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel.textColor = [UIColor whiteColor];
    //mainLabel.autoresizingMask =  UIViewAutoresizingFlexibleHeight;
    mainLabel.text = upgrade.name;
    mainLabel.shadowColor = [UIColor blackColor];
    mainLabel.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel];
    
    qtyLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 70, 3.0, 120.0, 15.0)];
    qtyLabel.tag = QTYLABEL_TAG;
    qtyLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    qtyLabel.textColor = [UIColor whiteColor];
    //qtyLabel.autoresizingMask =  UIViewAutoresizingFlexibleHeight;
    qtyLabel.text = [NSString stringWithFormat:@"Owned: %@", upgrade.quantity];
    [cell.contentView addSubview:qtyLabel];
    
    
    
    detailTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 18.0, 260.0, 15.0)];
    detailTextLabel.tag = SECONDLABEL_TAG;
    detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-light" size:11];
    detailTextLabel.textColor = [UIColor whiteColor];
    //detailTextLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    detailTextLabel.text = upgrade.description;
    [cell.contentView addSubview:detailTextLabel];
    
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];

    costLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 33.0, 220.0, 15.0)];
    costLabel.tag = COSTLABEL_TAG;
    costLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    costLabel.textColor = [UIColor yellowColor];
    //costLabel.autoresizingMask =  UIViewAutoresizingFlexibleHeight;
    costLabel.text = [NSString stringWithFormat:@"Cost: %@", [format stringFromNumber:upgrade.cost]];
    [cell.contentView addSubview:costLabel];

    ppsLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 48.0, 220.0, 12.0)];
    ppsLabel.tag = PPSLABEL_TAG;
    ppsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    ppsLabel.textColor = [UIColor yellowColor];
    //ppsLabel.autoresizingMask =  UIViewAutoresizingFlexibleHeight;
    ppsLabel.text = [NSString stringWithFormat:@"+%@ pps", [format stringFromNumber:upgrade.basePPS]];
    [cell.contentView addSubview:ppsLabel];

    
    photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 45.0)];
    photo.tag = PHOTO_TAG;
    photo.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    photo.image = [UIImage imageNamed:upgrade.icon];
    photo.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:photo];
    
}

-(void) updateTableCellRows:(UITableViewCell *)cell upgrade:(GameUpgrade *)upgrade
{
    //NSLog(@"update %@", upgrade.name);
    
    //mainLabel = [[UILabel alloc][cell.contentView viewWithTag:MAINLABEL_TAG]
    //[[UILabel alloc] in [cell.contentView viewWithTag:MAINLABEL_TAG].text = @"new"];
    for (UILabel *label in cell.contentView.subviews) {
        NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
        [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [format setNumberStyle:NSNumberFormatterDecimalStyle];
        if (label.tag == COSTLABEL_TAG) {
            label.text = [NSString stringWithFormat:@"Cost: %@", [format stringFromNumber:upgrade.cost]];
        } else if(label.tag == QTYLABEL_TAG){
            label.text = [NSString stringWithFormat:@"Owned: %@", upgrade.quantity];
        } else if(label.tag == SECONDLABEL_TAG){
            label.text = upgrade.description;
        } else if(label.tag == MAINLABEL_TAG) {
            label.text = upgrade.name;
        } else if(label.tag == PPSLABEL_TAG) {
            label.text = [NSString stringWithFormat:@"+%@ pps", [format stringFromNumber:upgrade.basePPS]];
        }
    
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithWhite:0 alpha:.9];
    
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, tableView.frame.size.width, 30)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]];
    [label setTextColor:[UIColor whiteColor]];
    NSString *string = @"Upgrades"; //[list objectAtIndex:section];
    if(section == 1) {
        string = @"Purchase Extras";
    }
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(1, 2);
    
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    
     return view;
}

-(void) closeView
{
    CGRect bounds = self.bounds;
    [UIView animateWithDuration:.15f
                     animations:^{
                         [self setFrame:CGRectMake(0,  bounds.size.height + 100, bounds.size.width, bounds.size.height - 100)];
                     }];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [_table cellForRowAtIndexPath:indexPath];
    NSLog(@"%d", cell.tag);
    
    if(cell.tag != 999) {
        //NSLog(@"%@", indexPath); // you can see selected row number in your console;
        cell.tag = 999;
        GameUpgrade *upgrade = [_gameModel.gameUpgrades objectAtIndex:indexPath.row];
        //NSLog(@"%d %@", indexPath.row, upgrade.name);
        if ([_gameModel doUpgrade:upgrade]) {
             [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        }
        cell.tag = 0;
    }
}



@end
