//
//  MyScene.h
//  PennyClick
//

//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <AVFoundation/AVFoundation.h>
#import <iAd/iAd.h>
#import <StoreKit/StoreKit.h>

@interface MyScene : SKScene 

//@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) SKAction *coinSound;
@property (strong, nonatomic) SKAction *clickSound;
@property (strong, nonatomic) SKAction *cashRegisterSound;




@end
