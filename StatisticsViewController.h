//
//  StatisticsViewController.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/19/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <UIKit/UIKit.h>
#define PIB_TAG 1
#define PPS_TAG 2
#define LTP_TAG 3
#define NUMCLICKS_TAG 4
#define PEPC_TAG 5
#define PPSLABEL_TAG 6
#define INVESTEDLABEL_TAG 7
#define UPPSLABEL_TAG 8
#define GENLABEL_TAG 9
#define OWNED_TAG 10
#define PBD_TAG 11
#define PBDLV_TAG 12

@interface StatisticsViewController : UIView <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property UITableView *table;

@end
