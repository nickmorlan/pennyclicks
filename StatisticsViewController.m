//
//  StatisticsViewController.m
//  PennyClick
//
//  Created by Nicholas Morlan on 5/19/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import "StatisticsViewController.h"
#import "GameModel.h"

@implementation StatisticsViewController
{
    GameModel *_gameModel;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    CGRect bounds = self.bounds;
    [self setBackgroundColor:[UIColor clearColor]];
    [self setFrame:CGRectMake(0,  bounds.size.height, bounds.size.width, bounds.size.height - 100)];
    
    // set up top border
    UIImage *bg = [[UIImage imageNamed:@"bordertop"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 40)];
    [border setBackgroundColor:[UIColor colorWithPatternImage:bg]];
    [self addSubview:border];
    
    // set up upgrades table view
    _table = [[UITableView alloc] initWithFrame:frame];
    _table.delegate = self;
    _table.dataSource = self;
    _table.separatorColor = [UIColor colorWithWhite:1 alpha:.2];
    _table.pagingEnabled = YES;
    _table.allowsMultipleSelection = NO;
    
    int adView = (UI_USER_INTERFACE_IDIOM () == UIUserInterfaceIdiomPad) ? 66 : 50;
    [_table setFrame:CGRectMake(0,  _table.frame.origin.y + 40, bounds.size.width, bounds.size.height - 140 - adView)];
    [self addSubview:_table];
    
    // close button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(bounds.size.width - 50, 5.0, 30.0, 30.0)];
    [button setBackgroundImage:[UIImage imageNamed:@"buttonclose"] forState:UIControlStateNormal];
    button.hidden = NO;
    [button setBackgroundColor:[UIColor clearColor]];
    [button addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchDown];
    [self addSubview:button];

    // initialize GameModel instance
    _gameModel = [GameModel sharedManager];
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (section == 0) {
        return 1;
    } else {
        return [_gameModel.gameUpgrades count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier0 = @"Cell0";
    static NSString *CellIdentifier1 = @"Cell1";
    UITableViewCell *cell;
    
    
    if(indexPath.section == 0) {
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
            cell.textLabel.textColor = [UIColor whiteColor];
            [self configureOverviewTableCellRows:cell];
       } else {
           cell = [self.table dequeueReusableCellWithIdentifier:CellIdentifier0];
           [self updateOverviewTableCellRows:cell];

       }
    
    } else if (indexPath.section == 1) {
        GameUpgrade *upgrade = [_gameModel.gameUpgrades objectAtIndex:indexPath.row];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
            cell.textLabel.textColor = [UIColor whiteColor];
            [self configureTableCellRows:cell upgrade:upgrade];
        } else {
            cell = [self.table dequeueReusableCellWithIdentifier:CellIdentifier1];
            [self updateTableCellRows:cell upgrade:upgrade];
        }
    }
    return cell;
    
}



#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        return 240;
    } else {
        return 70;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

-(void) configureOverviewTableCellRows:(UITableViewCell *)cell
{
    UILabel *mainLabel, *qtyLabel, *mainLabel1, *qtyLabel1, *mainLabel2, *qtyLabel2, *mainLabel3, *qtyLabel3, *mainLabel4, *qtyLabel4, *mainLabel5, *qtyLabel5, *mainLabel6, *qtyLabel6;
    UIImageView *photo;
    
    //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
   
    mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 3.0, 225.0, 16.0)];
    mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel.textColor = [UIColor whiteColor];
    mainLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel.text = @"Pennies in Penny Bank";
    mainLabel.shadowColor = [UIColor blackColor];
    mainLabel.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel];
    
    qtyLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 18.0, 225.0, 15.0)];
    qtyLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    qtyLabel.textColor = [UIColor yellowColor];
    qtyLabel.tag = PIB_TAG;
    qtyLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    qtyLabel.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.pennyBank]]];
    [cell.contentView addSubview:qtyLabel];
    
    mainLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 36.0, 225.0, 16.0)];
    mainLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel1.textColor = [UIColor whiteColor];
    mainLabel1.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel1.text = @"Pennies per Second";
    mainLabel1.shadowColor = [UIColor blackColor];
    mainLabel1.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel1];
    
    qtyLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 51.0, 225.0, 15.0)];
    qtyLabel1.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    qtyLabel1.textColor = [UIColor yellowColor];
    qtyLabel1.tag = PPS_TAG;
    qtyLabel1.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    if(_gameModel.basePenniesPerSecond > 1000) {
        qtyLabel1.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:floor(_gameModel.basePenniesPerSecond)]]];
    } else {
        qtyLabel1.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithFloat:_gameModel.basePenniesPerSecond]]];
        
    }
    [cell.contentView addSubview:qtyLabel1];
    
    mainLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 69.0, 225.0, 15.0)];
    mainLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel2.textColor = [UIColor whiteColor];
    mainLabel2.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel2.text = @"Lifetime Pennies Earned";
    mainLabel2.shadowColor = [UIColor blackColor];
    mainLabel2.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel2];
    
    qtyLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 84.0, 225.0, 15.0)];
    qtyLabel2.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    qtyLabel2.textColor = [UIColor yellowColor];
    qtyLabel2.tag = LTP_TAG;
    qtyLabel2.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    qtyLabel2.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.lifetimePennies]]];
    [cell.contentView addSubview:qtyLabel2];
    
    mainLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 102.0, 225.0, 15.0)];
    mainLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel3.textColor = [UIColor whiteColor];
    mainLabel3.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel3.text = @"Number of Clicks";
    mainLabel3.shadowColor = [UIColor blackColor];
    mainLabel3.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel3];
    
    qtyLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 117.0, 225.0, 15.0)];
    qtyLabel3.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    qtyLabel3.textColor = [UIColor yellowColor];
    qtyLabel3.tag = NUMCLICKS_TAG;
    qtyLabel3.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    qtyLabel3.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.totalClicks]]];
    [cell.contentView addSubview:qtyLabel3];
    
    mainLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 135.0, 225.0, 15.0)];
    mainLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel4.textColor = [UIColor whiteColor];
    mainLabel4.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel4.text = @"Pennies Earned From Clicks";
    mainLabel4.shadowColor = [UIColor blackColor];
    mainLabel4.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel4];
    
    qtyLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 150.0, 225.0, 15.0)];
    qtyLabel4.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    qtyLabel4.textColor = [UIColor yellowColor];
    qtyLabel4.tag = PEPC_TAG;
    qtyLabel4.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    qtyLabel4.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.lifetimePenniesFromClicks]]];
    [cell.contentView addSubview:qtyLabel4];
    
    mainLabel5 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 168.0, 225.0, 15.0)];
    mainLabel5.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel5.textColor = [UIColor whiteColor];
    mainLabel5.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel5.text = @"Penny Bags Collected";
    mainLabel5.shadowColor = [UIColor blackColor];
    mainLabel5.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel5];
    
    qtyLabel5 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 183.0, 225.0, 15.0)];
    qtyLabel5.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    qtyLabel5.textColor = [UIColor yellowColor];
    qtyLabel5.tag = PBD_TAG;
    qtyLabel5.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    qtyLabel5.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.pennyBagDrops]]];
    [cell.contentView addSubview:qtyLabel5];
    
    mainLabel6 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 201.0, 225.0, 15.0)];
    mainLabel6.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel6.textColor = [UIColor whiteColor];
    mainLabel6.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel6.text = @"Penny Bags Collected";
    mainLabel6.shadowColor = [UIColor blackColor];
    mainLabel6.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel6];
    
    qtyLabel6 = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 216.0, 225.0, 15.0)];
    qtyLabel6.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    qtyLabel6.textColor = [UIColor yellowColor];
    qtyLabel6.tag = PBDLV_TAG;
    qtyLabel6.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    qtyLabel6.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.pennyBagLifetimeValue]]];
    [cell.contentView addSubview:qtyLabel6];
    
    
    photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 45.0)];
    photo.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    photo.image = [UIImage imageNamed:@"penny"];
    photo.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:photo];
    
}

-(void) configureTableCellRows:(UITableViewCell *)cell upgrade:(GameUpgrade *)upgrade
{
    UILabel *mainLabel, *qtyLabel, *costLabel, *ppsLabel;
    UIImageView *photo;
   
    //NSLog(@"draw %@", upgrade.name);
    mainLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 3.0, 175.0, 15.0)];
    //mainLabel.tag = MAINLABEL_TAG;
    mainLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    mainLabel.textColor = [UIColor whiteColor];
    mainLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    mainLabel.text = upgrade.name;
    mainLabel.shadowColor = [UIColor blackColor];
    mainLabel.shadowOffset = CGSizeMake(1, 1);
    [cell.contentView addSubview:mainLabel];
    
    qtyLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 80, 3.0, 120.0, 15.0)];
    qtyLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:14];
    qtyLabel.textColor = [UIColor whiteColor];
    qtyLabel.tag = OWNED_TAG;
    qtyLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    qtyLabel.text = [NSString stringWithFormat:@"Owned: %@", upgrade.quantity];
    [cell.contentView addSubview:qtyLabel];
    
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [format setNumberStyle:NSNumberFormatterDecimalStyle];
    
    costLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 18.0, 220.0, 15.0)];
    costLabel.tag = INVESTEDLABEL_TAG;
    costLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    costLabel.textColor = [UIColor yellowColor];
    costLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    costLabel.text = [NSString stringWithFormat:@"Invested: %@", [format stringFromNumber:upgrade.penniesInvested]];
    [cell.contentView addSubview:costLabel];
    
    ppsLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 33.0, 220.0, 12.0)];
    ppsLabel.tag = UPPSLABEL_TAG;
    ppsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    ppsLabel.textColor = [UIColor yellowColor];
    ppsLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    float pps = [upgrade.basePPS floatValue] * [upgrade.quantity integerValue];
    if (pps > 100) {
        pps = floorf(pps);
    }
    ppsLabel.text = [NSString stringWithFormat:@"+%@ pps", [format stringFromNumber:[NSNumber numberWithFloat:pps]]];
    [cell.contentView addSubview:ppsLabel];
    
    ppsLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0, 48.0, 220.0, 12.0)];
    ppsLabel.tag = GENLABEL_TAG;
    ppsLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:12];
    ppsLabel.textColor = [UIColor yellowColor];
    ppsLabel.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
    ppsLabel.text = [NSString stringWithFormat:@"Generated: %@", [format stringFromNumber:upgrade.penniesGenerated]];
    [cell.contentView addSubview:ppsLabel];
    
    photo = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, 60.0, 45.0)];
    photo.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    photo.image = [UIImage imageNamed:upgrade.icon];
    photo.contentMode = UIViewContentModeScaleAspectFit;
    [cell.contentView addSubview:photo];
}

-(void) updateOverviewTableCellRows:(UITableViewCell *)cell
{
    
    //[[UILabel alloc] in [cell.contentView viewWithTag:MAINLABEL_TAG].text = @"new"];
    for (UILabel *label in cell.contentView.subviews) {
        NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
        [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [format setNumberStyle:NSNumberFormatterDecimalStyle];

        switch (label.tag) {
            case PIB_TAG:
                label.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.pennyBank]]];
                break;
            case PPS_TAG:
                if(_gameModel.basePenniesPerSecond > 1000) {
                    label.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:floor(_gameModel.basePenniesPerSecond)]]];
                } else {
                    label.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithFloat:_gameModel.basePenniesPerSecond]]];
                }
                break;
            case LTP_TAG:
                label.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.lifetimePennies]]];
                break;
            case NUMCLICKS_TAG:
                label.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.totalClicks]]];
                break;
            case PEPC_TAG:
                label.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:[NSNumber numberWithUnsignedLongLong:_gameModel.lifetimePenniesFromClicks]]];
                break;
                
            default:
                break;
        }
        
    }
}

-(void) updateTableCellRows:(UITableViewCell *)cell upgrade:(GameUpgrade *)upgrade
{
    //NSLog(@"update %@", upgrade.name);
    
    //mainLabel = [[UILabel alloc][cell.contentView viewWithTag:MAINLABEL_TAG]
    //[[UILabel alloc] in [cell.contentView viewWithTag:MAINLABEL_TAG].text = @"new"];
    for (UILabel *label in cell.contentView.subviews) {
        NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
        [format setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [format setNumberStyle:NSNumberFormatterDecimalStyle];
        
       switch (label.tag) {
            case INVESTEDLABEL_TAG:
                label.text = [NSString stringWithFormat:@"%@", [format stringFromNumber:upgrade.penniesInvested]];
               break;
            case UPPSLABEL_TAG: {
                float pps = [upgrade.basePPS floatValue] * [upgrade.quantity integerValue];
                if (pps > 100) {
                    pps = floorf(pps);
                }
                label.text = [NSString stringWithFormat:@"+%@ pps", [format stringFromNumber:[NSNumber numberWithFloat:pps]]];
                break;
            }
            case GENLABEL_TAG:
                label.text = [NSString stringWithFormat:@"Generated: %@", [format stringFromNumber:upgrade.penniesGenerated]];
                break;
            case OWNED_TAG:
                label.text = [NSString stringWithFormat:@"Owned: %@", upgrade.quantity];
                break;
                
            default:
                break;
        }
        
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithWhite:0 alpha:.9];
    

    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, tableView.frame.size.width, 30)];
    [label setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]];
    [label setTextColor:[UIColor whiteColor]];
    NSString *string = @"Game Stats";
    if(section == 1) {
        string = @"Upgradables Stats";
    }
    label.shadowColor = [UIColor blackColor];
    label.shadowOffset = CGSizeMake(1, 2);
    
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    
    return view;
}

-(void) closeView
{
    CGRect bounds = self.bounds;
    [UIView animateWithDuration:.15f
                     animations:^{
                         [self setFrame:CGRectMake(0,  bounds.size.height + 100, bounds.size.width, bounds.size.height - 100)];
                     }];
    
}


- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell.selectionStyle == UITableViewCellSelectionStyleNone){
        return nil;
    }
    return indexPath;
}


@end
