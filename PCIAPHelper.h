//
//  JBDIAPHelper.h
//  JumpyBulldog
//
//  Created by Nicholas Morlan on 4/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "IAPHelper.h"

@interface PCIAPHelper : IAPHelper

+ (PCIAPHelper *)sharedInstance;

@end
