//
//  ViewController.h
//  PennyClick
//

//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>
#import "UpgradesViewController.h"
#import "StatisticsViewController.h"
#import "BonusViewController.h"

@interface ViewController : UIViewController 

@property (nonatomic, strong) UpgradesViewController *upgradesTable;
@property (nonatomic, strong) StatisticsViewController *statisticsTable;
@property (nonatomic, strong) BonusViewController *bonusTable;

-(void) closeView;
-(void) showUpgradesTable;
-(void) showStatisticsTable;
-(void) showBonusTable;
-(void) updateStatisticsTable;

@end
