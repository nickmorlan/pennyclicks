//
//  Penny.h
//  PennyClick
//
//  Created by Nicholas Morlan on 5/13/14.
//  Copyright (c) 2014 Nicholas Morlan. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Penny : SKSpriteNode

@property bool touched;

-(id) initWithDoubleClicker;
-(BOOL) isTouched;

@end
