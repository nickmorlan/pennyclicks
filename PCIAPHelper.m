//
//  JBDIAPHelper.m
//  JumpyBulldog
//
//  Created by Nicholas Morlan on 4/29/14.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "PCIAPHelper.h"
#import "GameModel.h"

@implementation PCIAPHelper

+ (PCIAPHelper *)sharedInstance {
    static dispatch_once_t once;
    static PCIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.r5kmedia.PennyClicks.doubleclicker",
                                      @"com.r5kmedia.PennyClicks.moneybag",
                                      @"com.r5kmedia.PennyClicks.piggybank",
                                      nil];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}


- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"1restoreTransaction...");
    
    [super provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.r5kmedia.PennyClicks.doubleclicker" object:transaction.originalTransaction.payment.productIdentifier userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"playCashRegister" object:nil];
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    
    [super provideContentForProductIdentifier:transaction.payment.productIdentifier];
    
    GameModel *gameModel = [GameModel sharedManager];
    if([transaction.payment.productIdentifier isEqualToString:@"com.r5kmedia.PennyClicks.doubleclicker"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.r5kmedia.PennyClicks.doubleclicker" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playCashRegister" object:nil];
       gameModel.doubleClicker = YES;
        gameModel.clickMultiplier = 2;
    } else if ([transaction.payment.productIdentifier isEqualToString:@"com.r5kmedia.PennyClicks.piggybank"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playCashRegister" object:nil];
       [gameModel addToPennyBank:1507669098];
    } else if ([transaction.payment.productIdentifier isEqualToString:@"com.r5kmedia.PennyClicks.moneybag"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"playCashRegister" object:nil];
       [gameModel addToPennyBank:1899332573098];
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}


@end
